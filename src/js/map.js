/**
 * ---------------------------------------
 * This demo was created using amCharts 4.
 *
 * For more information visit:
 * https://www.amcharts.com/
 *
 * Documentation is available at:
 * https://www.amcharts.com/docs/v4/
 * ---------------------------------------
 */

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create map instance
var chart = am4core.create("chartdiv", am4maps.MapChart);

// Set map definition
chart.geodata = am4geodata_worldLow;

// Set projection
chart.projection = new am4maps.projections.Miller();

// Create map polygon series
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Exclude Antartica
polygonSeries.exclude = ["AQ"];

// Make map load polygon (like country names) data from GeoJSON
polygonSeries.useGeodata = true;

// Configure series
var polygonTemplate = polygonSeries.mapPolygons.template;
//polygonTemplate.tooltipText = "{name}";
//polygonTemplate.polygon.fillOpacity = 0.6;
polygonTemplate.fill = am4core.color("#66CCCC");
polygonTemplate.stroke = am4core.color("#66CCCC");

// Create hover state and set alternative fill color
// var hs = polygonTemplate.states.create("hover");
// hs.properties.fill = chart.colors.getIndex(0);
// get lang
let url = new URL(window.location.href)
lang = url.searchParams.get('lang') ? url.searchParams.get('lang') : null
// console.log(lang)
// Add image series
var imageSeries = chart.series.push(new am4maps.MapImageSeries());
imageSeries.mapImages.template.propertyFields.longitude = "longitude";
imageSeries.mapImages.template.propertyFields.latitude = "latitude";
if (lang == "ch") {
  imageSeries.mapImages.template.tooltipText = "{title_ch}";
}
else{
  imageSeries.mapImages.template.tooltipText = "{title}";
}

imageSeries.mapImages.template.propertyFields.url = "url";

var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
circle.radius = 4;
circle.propertyFields.fill = "color";

var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
circle2.radius = 5;
circle2.propertyFields.fill = "color";

circle2.events.on("inited", function (event) {
  animateBullet(event.target);
});

function animateBullet(circle) {
  var animation = circle.animate(
    [
      { property: "scale", from: 1, to: 5 },
      { property: "opacity", from: 1, to: 0 },
    ],
    1000,
    am4core.ease.circleOut
  );
  animation.events.on("animationended", function (event) {
    animateBullet(event.target.object);
  });
}

var colorSet = new am4core.ColorSet();

imageSeries.data = [
  {
    title: "U.S.A.",
    title_ch: "美国",
    latitude: 37.09024,
    longitude: -95.712891,
    color: colorSet.next(),
  },
  {
    title: "China",
    title_ch: "中国",
    latitude: 35.86166,
    longitude: 104.195396,
    color: colorSet.next(),
  },
  {
    title: "Hong Kong",
    title_ch: "中国相关",
    latitude: 22.380341,
    longitude: 114.141296,
    color: colorSet.next(),
  },
  {
    title: "Netherlands",
    title_ch: "荷兰",
    latitude: 52.132633,
    longitude: 5.291266,
    color: colorSet.next(),
  },
  {
    title: "Singapore",
    title_ch: "新加坡",
    latitude: 1.352083,
    longitude: 103.819839,
    color: colorSet.next(),
  },
  {
    title: "Reunion",
    title_ch: "留尼汪",
    latitude: -20.878901,
    longitude: 55.448101,
    color: colorSet.next(),
  },
  {
    title: "South Africa",
    title_ch: "南非",
    latitude: -28.48322,
    longitude: 24.676997,
    color: colorSet.next(),
  },
  {
    title: "Switzerland",
    title_ch: "瑞士",
    latitude: 46.204391,
    longitude: 6.143158,
    color: colorSet.next(),
  },
  {
    title: "New Zealand",
    title_ch: "新西兰",
    latitude: -40.900557,
    longitude: 174.885971,
    color: colorSet.next(),
  },
  {
    title: "Belgium",
    title_ch: "比利时",
    latitude: 50.503887,
    longitude: 4.469936,
    color: colorSet.next(),
  },
  {
    title: "Israel",
    title_ch: "以色列",
    latitude: 31.046051,
    longitude: 34.851612,
    color: colorSet.next(),
  },
  {
    title: "Taiwan",
    title_ch: "中国台湾",
    latitude: 23.69781,
    longitude: 120.960515,
    color: colorSet.next(),
  },
  {
    title: "Canada",
    title_ch: "加拿大",
    latitude: 56.130366,
    longitude: -106.346771,
    color: colorSet.next(),
  },
  {
    title: "Poland",
    title_ch: "波兰",
    latitude: 51.919438,
    longitude: 19.145136,
    color: colorSet.next(),
  },
  {
    title: "Spain",
    title_ch: "西班牙",
    latitude: 40.463667,
    longitude: -3.74922,
    color: colorSet.next(),
  },
  {
    title: "France",
    title_ch: "法国",
    latitude: 46.227638,
    longitude: 2.213749,
    color: colorSet.next(),
  },
  {
    title: "Germany",
    title_ch: "德国",
    latitude: 51.165691,
    longitude: 10.451526,
    color: colorSet.next(),
  },
  {
    title: "Côte d'Ivoire",
    title_ch: "象牙海岸",
    latitude: 7.539989,
    longitude: -5.54708,
    color: colorSet.next(),
  },
  {
    title: "Australia",
    title_ch: "澳大利亚",
    latitude: -25.274398,
    longitude: 133.775136,
    color: colorSet.next(),
  },
  {
    title: "Guadeloupe",
    title_ch: "瓜德罗普",
    latitude: 16.995971,
    longitude: -62.067641,
    color: colorSet.next(),
  },
  {
    title: "Italy",
    title_ch: "意大利",
    latitude: 41.87194,
    longitude: 12.56738,
    color: colorSet.next(),
  },
  {
    title: "Angola",
    title_ch: "安哥拉",
    latitude: -11.202692,
    longitude: 17.873887,
    color: colorSet.next(),
  },
  {
    title: "Jamaica",
    title_ch: "牙买加",
    latitude: 18.109581,
    longitude: -77.297508,
    color: colorSet.next(),
  },
  {
    title: "Bahrain",
    title_ch: "巴林",
    latitude: 25.930414,
    longitude: 50.637772,
    color: colorSet.next(),
  },
  {
    title: "Martinique",
    title_ch: "马提尼克",
    latitude: 14.641528,
    longitude: -61.024174,
    color: colorSet.next(),
  },
  {
    title: "Hungary",
    title_ch: "匈牙利",
    latitude: 47.162494,
    longitude: 19.503304,
    color: colorSet.next(),
  },
  {
    title: "Brazil",
    title_ch: "巴西",
    latitude: -14.235004,
    longitude: -51.92528,
    color: colorSet.next(),
  },
  {
    title: "Botswana",
    title_ch: "博茨瓦纳",
    latitude: -22.328474,
    longitude: 24.684866,
    color: colorSet.next(),
  },
  {
    title: "Portugal",
    title_ch: "葡萄牙",
    latitude: 39.399872,
    longitude: -8.224454,
    color: colorSet.next(),
  },
  {
    title: "Saudi Arabia",
    title_ch: "沙特阿拉伯",
    latitude: 23.885942,
    longitude: 45.079162,
    color: colorSet.next(),
  },
  {
    title: "Congo",
    title_ch: "刚果",
    latitude: -0.6620705,
    longitude: 14.927424,
    color: colorSet.next(),
  },
  {
    title: "Iceland",
    title_ch: "冰岛",
    latitude: 64.963051,
    longitude: -19.020835,
    color: colorSet.next(),
  },
  {
    title: "Philippines",
    title_ch: "菲律宾",
    latitude: 12.879721,
    longitude: 121.774017,
    color: colorSet.next(),
  },
  {
    title: "Kuwait",
    title_ch: "科威特",
    latitude: 29.31166,
    longitude: 47.481766,
    color: colorSet.next(),
  },
];

// Zoom control
chart.zoomControl = new am4maps.ZoomControl();

var homeButton = new am4core.Button();
homeButton.events.on("hit", function () {
  chart.goHome();
});

homeButton.icon = new am4core.Sprite();
homeButton.padding(7, 5, 7, 5);
homeButton.width = 30;
homeButton.icon.path =
  "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
homeButton.marginBottom = 10;
homeButton.parent = chart.zoomControl;
homeButton.insertBefore(chart.zoomControl.plusButton);
