# Riceland

## Configuration
node 14

## How to Dev
**Always** npm run dev before developing

If you want to make change to UI do it in src folder

If you want to make php changes, do it in public/\*.php or public/util/\*.php

**DO NOT** Make any changes in **public** folder **EXCEPT** public/\*.php or public/util/\*.php

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
