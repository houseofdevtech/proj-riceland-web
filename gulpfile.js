"use strict";

var gulp = require("gulp"),
  watch = require("gulp-watch"),
  uglify = require("gulp-uglify"),
  sass = require("gulp-sass"),
  postcss = require("gulp-postcss"),
  mqpacker = require("css-mqpacker"),
  cleanCSS = require("gulp-clean-css"),
  autoprefixer = require("autoprefixer"),
  changed = require("gulp-changed"),
  reference = require("gulp-reference"),
  rename = require("gulp-rename"),
  sourcemaps = require("gulp-sourcemaps"),
  fileinclude = require("gulp-file-include"),
  imagemin = require("gulp-imagemin"),
  pngquant = require("imagemin-pngquant"),
  rimraf = require("rimraf"),
  path = {
    dist: {
      html: "dist/",
      js: "dist/js/",
      style: "dist/css/",
      img: "dist/img/",
      fonts: "dist/fonts/",
    },
    public: {
      tpl: "public/templates/",
      js: "public/js/",
      style: "public/css/",
      img: "public/img/",
      fonts: "public/fonts/",
    },
    src: {
      html: ["src/html/**/*.html", "!src/html/template/*.html"],
      tpl: "src/html/view/*.html",
      js: ["src/js/lib/device.js", "src/js/lib/jquery-2.2.4.js", "src/js/*.js"],
      style: "src/style/main.scss",
      img: "src/img/**/*.*",
      fonts: "src/fonts/**/*.*",
    },
    watch: {
      html: "src/html/**/*.html",
      js: "src/js/**/*.js",
      style: "src/style/**/*.{css,scss}",
      img: "src/img/**/*.*",
      fonts: "src/fonts/**/*.*",
    },
    // clean: "./dist",
    clean: "./public/templates",
  };

var postCssPlugins = [
  autoprefixer({
    overrideBrowserslist: ["last 25 versions", "> 1%", "ie 9"],
  }),
  mqpacker({
    sort: true,
  }),
];

gulp.task("clean", function (cb) {
  rimraf(path.clean, cb);
});

gulp.task("html:build", function () {
  return (
    gulp
      .src(path.src.html)
      .pipe(
        fileinclude({
          prefix: "@@",
          basepath: "@file",
          indent: true,
        })
      )
      // .pipe(rename({ extname: ".php" }))
      .pipe(gulp.dest(path.dist.html))
  );
});

gulp.task("js:build", function () {
  return (
    gulp
      .src(path.src.js)
      .pipe(sourcemaps.init())
      .pipe(reference())
      // .pipe(gulp.dest(path.dist.js))
      .pipe(gulp.dest(path.public.js))
      .pipe(uglify())
      .pipe(rename({ suffix: ".min" }))
      .pipe(sourcemaps.write("."))
      // .pipe(gulp.dest(path.dist.js));
      .pipe(gulp.dest(path.public.js))
  );
});

gulp.task("style:build", function () {
  return (
    gulp
      .src(path.src.style)
      .pipe(sourcemaps.init())
      .pipe(reference())
      .pipe(sass({ outputStyle: "compact" }).on("error", sass.logError))
      .pipe(
        cleanCSS({
          format: "beautify",
          level: 2,
        })
      )
      .pipe(postcss(postCssPlugins))
      .pipe(rename({ basename: "style" }))
      // .pipe(gulp.dest(path.dist.style))
      .pipe(gulp.dest(path.public.style))
      .pipe(cleanCSS())
      .pipe(rename({ suffix: ".min" }))
      .pipe(sourcemaps.write("."))
      // .pipe(gulp.dest(path.dist.style))
      .pipe(gulp.dest(path.public.style))
  );
});

gulp.task("image:build", function () {
  return (
    gulp
      .src(path.src.img)
      .pipe(
        imagemin({
          optimizationLevel: 5,
          progressive: true,
          svgoPlugins: [{ removeViewBox: false }],
          use: [pngquant()],
          interlaced: true,
        })
      )
      // .pipe(gulp.dest(path.dist.img));
      .pipe(gulp.dest(path.public.img))
  );
});

gulp.task("fonts:build", function () {
  return (
    gulp
      .src(path.src.fonts)
      .pipe(changed(path.dist.fonts))
      // .pipe(gulp.dest(path.dist.fonts));
      .pipe(gulp.dest(path.public.fonts))
  );
});

gulp.task("js:dev", function () {
  return (
    gulp
      .src(path.src.js)
      .pipe(sourcemaps.init())
      .pipe(reference())
      .pipe(rename({ suffix: ".min" }))
      .pipe(sourcemaps.write("."))
      // .pipe(gulp.dest(path.dist.js));
      .pipe(gulp.dest(path.public.js))
  );
});

gulp.task("style:dev", function () {
  return (
    gulp
      .src(path.src.style)
      .pipe(sourcemaps.init())
      .pipe(reference())
      .pipe(sass().on("error", sass.logError))
      .pipe(postcss(postCssPlugins))
      .pipe(
        cleanCSS({
          format: "beautify",
          level: 2,
        })
      )
      .pipe(
        rename({
          basename: "style",
          suffix: ".min",
        })
      )
      .pipe(sourcemaps.write("."))
      // .pipe(gulp.dest(path.dist.style));
      .pipe(gulp.dest(path.public.style))
  );
});

gulp.task("image:dev", function () {
  return (
    gulp
      .src(path.src.img)
      .pipe(changed(path.dist.img))
      // .pipe(gulp.dest(path.dist.img));
      .pipe(gulp.dest(path.public.img))
  );
});

gulp.task("tpl:build", function () {
  return gulp
    .src(path.src.tpl)
    .pipe(
      fileinclude({
        prefix: "@@",
        basepath: "@file",
        indent: true,
      })
    )
    .pipe(rename({ extname: ".tpl" }))
    .pipe(gulp.dest(path.public.tpl));
});

gulp.task(
  "build",
  gulp.series(
    "clean",
    gulp.parallel(
      // "html:build",
      "tpl:build",
      "js:build",
      "style:build",
      "fonts:build",
      "image:build"
    )
  )
);

gulp.task(
  "dev",
  gulp.series(
    "clean",
    gulp.parallel(
      // "html:build",
      "tpl:build",
      "js:dev",
      "style:dev",
      "fonts:build",
      "image:dev"
    )
  )
);

gulp.task("default", gulp.series("build"));

// gulp.task("watch", function (_cb) {
//   watch(path.watch.html, gulp.series("html:build", "tpl:build"));
//   watch(path.watch.style, gulp.series("style:dev"));
//   watch(path.watch.js, gulp.series("js:dev"));
//   watch(path.watch.img, gulp.series("image:dev"));
//   watch(path.watch.fonts, gulp.series("fonts:build"));

//   _cb();
// });
gulp.task("watch", function (_cb) {
  watch(path.watch.html, gulp.series("tpl:build"));
  watch(path.watch.style, gulp.series("style:dev"));
  watch(path.watch.js, gulp.series("js:dev"));
  watch(path.watch.img, gulp.series("image:dev"));
  watch(path.watch.fonts, gulp.series("fonts:build"));

  _cb();
});
