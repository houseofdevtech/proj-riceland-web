<?php
/**
*	Common Database Accessing Object
* 	Created 20-5-2012
* */
	
	/*Database object configuration file*/
	require_once(dirname(__FILE__).'/'.'database_object_config.php');
	
	/*Internal Lib Require*/
	require_once(dirname(__FILE__).'/'.'../standard_objects/standard_object.php');
	//require_once(dirname(__FILE__).'/'.'../cached_objects/memcached_object.php');
	
	/*External Lib Require*/
	//define('ADODB_ASSOC_CASE', 0);
	/*	0 = assoc lowercase field names. $rs->fields['orderid']
		1 = assoc uppercase field names. $rs->fields['ORDERID']
		2 = use native-case field names. $rs->fields['OrderID']	*/
	require_once(dirname(__FILE__).'/'.'../external_lib_core/adodb/adodb.inc.php');
		
	class database_object extends standard_object {

		private $_config_table_name;
		
		private $_config_primarykey_member;
		
		private $_config_member_to_field;
		private $_config_field_to_member;
		private $_config_member_datatype;
		
		private $_config_field_prefix;
		private $_config_field_firstpart_replace;
		
		private $_config_table_database_engine;
		private $_config_use_insert_delayed;
		
		private $_config_max_arraymember_size;
		private $_config_load_all_arraymember;
		
		function database_object($table_name='', $field_prefix='', $field_firstpart_replace='') {
			
			$this->_config_member_to_field = array();
			$this->_config_field_to_member = array();
			
			$this->_config_table_name 			= $table_name;

			$this->_config_field_prefix 			= $field_prefix;
			$this->_config_field_firstpart_replace	= $field_firstpart_replace;
			
			$this->_config_use_insert_delayed = 0;
			
			$this->_config_max_arraymember_size = 30;
			$this->_config_load_all_arraymember = 0;
			
			/*config memcache object*/
			if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
				if (empty($this->_config_memcache_key_prefix) == FALSE) {		
					parent::memcached_object(DATABASE_HOST.'.'.DATABASE_NAME.'.'.$this->_config_memcache_key_prefix);
				} else {
					parent::memcached_object(DATABASE_HOST.'.'.DATABASE_NAME);
				} 
			} else {
				parent::standard_object();
			}
			
			/*preventing checking array member error on standard_object constructor*/
			$this->create_association_array();
		}
		
		function connect_database(&$db_instance) {
						
			//$db_instance = NewADOConnection(DATABASE_ENGINE);
			$db_instance = NewADOConnection('mysqli');
			$db_instance->Connect(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME);
			$db_instance->Execute("SET NAMES 'utf8';");	
			
			return $db_instance;
		}
		
		protected function check_table_existence($table_name) {

			$sql = "SHOW TABLES LIKE '".$table_name."'";
			$db_instance = $this->connect_database($db_instance);	
			$loaded_row = $db_instance->GetRow($sql);
			$db_instance->Close();
			unset($db_instance);

			if($loaded_row[0] == NULL){
				return FALSE;
			}
			else{
				return TRUE;
			}
		}

		private function create_association_array() {
			
			if (empty($this->_config_table_name) == TRUE) {
				return FALSE;
			}
			
			/*load associated array value from cache*/
			if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
				
				$this->_config_member_to_field 		= $this->load_custom_value('_config_member_to_field');
				$this->_config_field_to_member 		= $this->load_custom_value('_config_field_to_member');
				$this->_config_member_datatype 	= $this->load_custom_value('_config_member_datatype');
				$this->_config_primarykey_member 	= $this->load_custom_value('_config_primarykey_member');
				$this->_config_table_database_engine = $this->load_custom_value('_config_table_database_engine');
				
				if (	(is_array($this->_config_member_to_field) == TRUE) &&
					(is_array($this->_config_field_to_member) == TRUE) &&
					(is_array($this->_config_member_datatype) == TRUE) &&
					($this->_config_primarykey_member !== FALSE) ) {	
						return TRUE;
					}
			}
			
			$db_instance = $this->connect_database($db_instance);	
			$query_result = $db_instance->Execute('DESCRIBE '.$this->_config_table_name.';');
			
			if ($query_result === false) {
			
				$db_instance->Close();
				return FALSE;
			}
			
			while ($table_row = $query_result->FetchNextObject()) {
				
				$this_var = get_object_vars($this);
				
				foreach ($this_var as $name => $value) {
					
					if ($name == $table_row->FIELD) {
						$this->_config_member_to_field[$name] 				= $table_row->FIELD;
						$this->_config_field_to_member[$table_row->FIELD] 	= $name;
						$this->_config_member_datatype[$name]				= $table_row->TYPE;
						
						if ($table_row->KEY == 'PRI') {
							$this->_config_primarykey_member = $name;
						}
					} else
					if ( 	(empty($this->_config_field_prefix) == FALSE) && 
						($this->_config_field_prefix.'_'.$name == $table_row->FIELD) ) {
						
						$this->_config_member_to_field[$name] 				= $table_row->FIELD;
						$this->_config_field_to_member[$table_row->FIELD] 	= $name;
						$this->_config_member_datatype[$name]				= $table_row->TYPE;
						
						if ($table_row->KEY == 'PRI') {
							$this->_config_primarykey_member = $name;
						}						
					} else 
					if ( 	(empty($this->_config_field_firstpart_replace) == FALSE) &&
						(preg_replace('/^([a-zA-Z0-9]+)_/', $this->_config_field_firstpart_replace.'_',$name) == $table_row->FIELD)) {
					
						$this->_config_member_to_field[$name] 				= $table_row->FIELD;
						$this->_config_field_to_member[$table_row->FIELD] 	= $name;
						$this->_config_member_datatype[$name]				= $table_row->TYPE;
						
						if ($table_row->KEY == 'PRI') {
							$this->_config_primarykey_member = $name;
						}								
					}
				}
			}
			
			/*Checking Database Engine of the Table*/
			$row_data = $db_instance->GetAll('SHOW TABLE STATUS WHERE Name ="'.$this->_config_table_name.'";');
			
			if (count($row_data) == 0) {		
				$db_instance->Close();
				return FALSE;
			}
			$this->_config_table_database_engine = $row_data[0]['Engine'];

			$db_instance->Close();
			
			/*save associated array value to cache*/
			if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
				$this->save_custom_value('_config_member_to_field', $this->_config_member_to_field);
				$this->save_custom_value('_config_field_to_member', $this->_config_field_to_member);
				$this->save_custom_value('_config_member_datatype', $this->_config_member_datatype);
				$this->save_custom_value('_config_primarykey_member', $this->_config_primarykey_member);
				$this->save_custom_value('_config_table_database_engine', $this->_config_table_database_engine);
			}
			
			return TRUE;
		}
		
		function get_associated_table() {
			return $this->_config_table_name;
		}
		
		protected function can_insert_delayed() {
			
			if (strpos(strtolower(DATABASE_ENGINE), 'mysql') !== FALSE) {
				return FALSE;
			}
			
			$available_table_engine = array(
				'MyISAM',
				'MEMORY',
				'ARCHIVE',
				'BLACKHOLE'
			);
			
			$pattern_string = implode('|', $available_table_engine);
			
			if (preg_match('/'.$pattern_string.'/i', $this->_config_table_database_engine) > 0) {
				return TRUE;
			}
			
			return FALSE;
		}
		
		protected function can_replace_into() {
			
			if (strpos(strtolower(DATABASE_ENGINE), 'mysql') !== FALSE) {
				return TRUE;
			}
			return FALSE;
		}
		
		protected function get_primarykey_member_name() {
			return $this->_config_primarykey_member;
		}
				
		protected function get_associated_member($field_name) {
			
			if (empty($this->_config_field_to_member) == TRUE) {
				return FALSE;
			}
			
			return $this->_config_field_to_member[$field_name];
		}
		
		protected function get_associated_field($member_name) {
			
			if (empty($this->_config_field_to_member) == TRUE) {
				return FALSE;
			}			
			
			return $this->_config_member_to_field[$member_name];
		}
		
		protected function set_max_arraymember_size($size = 30) {
			$this->_config_max_arraymember_size = $size;
		}
		
		protected function set_load_all_arraymember($is_load_all = 0) {
			$this->_config_load_all_arraymember = $is_load_all;
		}
		
		function set_use_insert_delayed($use_insert_delayed = 0) {
			$this->_config_use_insert_delayed = $use_insert_delayed;
		}
		
		function get_cache_sql_offset() {
			if ($this->_config_have_arraymember == FALSE) {
				return '';
			}
			$sql_offset_where = '';
			$sql_offset_order	= '';
			$sql_offset_group	= '';
			$this_var = get_object_vars($this);
			foreach ($this_var as $object_name => $object_data) {
				if (preg_match('/_sql_where_hook$/', $object_name)) {
					/*delete line break*/
					$object_data = preg_replace('/\n/', '', $object_data);
					$sql_offset_where .= preg_replace('/\s/', '_', $object_data);
				}
				if (preg_match('/_sql_group_hook$/', $object_name)) {
					/*delete line break*/
					$object_data = preg_replace('/\n/', '', $object_data);					
					$sql_offset_group .= preg_replace('/\s/', '_', $object_data);
				}
				if (preg_match('/_sql_order_hook$/', $object_name)) {
					/*delete line break*/
					$object_data = preg_replace('/\n/', '', $object_data);					
					$sql_offset_order .= preg_replace('/\s/', '_', $object_data);
				}
			}
			
			return (empty($sql_offset_where) == FALSE ? '.'.$sql_offset_where : '') . (empty($sql_offset_group) == FALSE ? '.'.$sql_offset_group : '') . (empty($sql_offset_order) == FALSE ? '.'.$sql_offset_order : ''); 
		}		
		
		function check_database_connection() {
			
			if (empty($this->_config_table_name) == TRUE) {
				return FALSE;
			}
			
			$db_instance = $this->connect_database($db_instance);	
			
			try {
			
		 		$db_instance->Execute('DESCRIBE '.$this->_config_table_name.';');
				
			} catch (exception $error) {
			
				$db_instance->Close();
				print_r($error);
				return FALSE;
			}
			
			$db_instance->Close();
			return TRUE;
		}
		
		private function fill_sql_quote($member_name) {
				
			/*creating pattern string*/
			$pattern_array = array(
				'int\(',
				'decimal',
				'float',
				'double',
				'real',
				'bit',
				'boolean',
				'serial'
			);
			$pattern_string = implode('|', $pattern_array);
			
			if (preg_match('/'.$pattern_string.'/', $this->_config_member_datatype[$member_name]) > 0) {
				
				if (empty($this->$member_name) == TRUE) {
					return 0;
				}
				return $this->$member_name;
			}
			
			/*sql injection protection*/
			return '"'.preg_replace('/"/i', '\"', $this->$member_name).'"';
		}
		function get_sql_condition($member_name='', $condition='=') {
				
			if ($this->$member_name === '') {
				return '';
			}
			if (($this->$member_name != '0') && ($this->$member_name == ''))  {
				return '';
			}			
			if ($this->get_associated_field($member_name) == '') {
				return '';
			}
			if (empty($condition) == TRUE) {
				$condition = '=';
			}
			
			$sql_condition = $this->get_associated_field($member_name) . ' ' . $condition . ' ' . $this->fill_sql_quote($member_name);
				
			return $sql_condition;
		}
		
		function get_fields_and_values_list(&$field_list, &$value_list, $table_name, $is_save=0) {
			
			$have_value = FALSE;
			
			if ($table_name != $this->_config_table_name) {
				return TRUE;
			}
			
			foreach ($this->_config_member_to_field as $name => $value) {
				
				if (	($this->_config_primarykey_member != $name) ||
						( ($this->can_replace_into() == TRUE) && ($this->_config_primarykey_member == $name) && (empty($this->$name) == FALSE) ) || 
					($is_save == 0) ) {
					
					/*check for listing duplicate fields and values*/
					if (preg_match('/^'.$value.',$/', $field_list) == FALSE) {
						$field_list 	.= $value.',';
						$value_list 	.= $this->fill_sql_quote($name).',';							
					}
					
					/*check for protecting saving empty object*/
					if (empty($value) == FALSE) {
						$have_value = TRUE;
					}
				}
			}
			
			/*recursive call to object members*/
			if ($this->_config_have_objectmember == TRUE) {
				$this_vars = get_object_vars($this);
				foreach ($this_vars as $name => $value) {
								
					if ((is_object($this->$name) == TRUE) && (is_subclass_of($this->$name, 'database_object') == TRUE)) {
						$have_value &= $this->$name->get_fields_and_values_list($field_list, $value_list, $table_name, $is_save);
					}
				}				
			}

			
			return $have_value;
		}
		
		function set_values_from_fields($table_row, $table_name) {
		
			if ($table_name != $this->_config_table_name) {
				return FALSE;
			}
			
			/*filling properties of object properties from database fields*/
			$this_vars = get_object_vars($this);
			foreach ($this_vars as $name => $value) {
							
				if ((is_object($this->$name) == TRUE) && (is_subclass_of($this->$name, 'database_object') == TRUE)) {
					$this->$name->set_values_from_fields($table_row, $table_name);
				}
			}		

			/*get properties value from database fields*/
			$is_all_null = TRUE;
			$primary_key = $this->_config_primarykey_member;
			foreach ($this->_config_member_to_field as $name => $value) {

				if (($this->_config_primarykey_member != $name) || (empty($this->$primary_key) == TRUE)) {

					/*one of fields is not null*/
					if (is_null($table_row[$value]) == FALSE) {
						$is_all_null = FALSE;
					}

					/*set field value to properties*/
					$this->$name = $table_row[$value];
				}
			}		

			/*check for getting all null values*/
			if (($is_all_null == TRUE) && (property_exists($this, $this->$primary_key) == TRUE)) {
				$this->$primary_key = NULL;
				return FALSE;
			}
			
			return TRUE;			
		}
		
		protected function save_objectmember_with_primarykey() {
		
			if ($this->_config_have_objectmember == FALSE) {
				return FALSE;
			}
		
			$this_var = get_object_vars($this);
			foreach ($this_var as $name => $value) {
				$name_primary_key = '_config_'.$name.'_primary_key';
				
				if (	(is_object($this->$name) == TRUE) && 
					(is_subclass_of($this->$name, 'database_object') == TRUE) && 
		
					($this->$name->get_associated_table() != '') && 
					($this->$name->get_associated_table() != $this->_config_table_name) && 
					
					(property_exists($this, $name_primary_key) == TRUE) && 
					(empty($this->$name_primary_key) == FALSE)	&&
					($this->$name_primary_key != $this->_config_primarykey_member) ) {
					
					$current_primary_key = $this->$name_primary_key;
					
					if (	(property_exists($this, $current_primary_key) == TRUE) && 
						(empty($this->$current_primary_key) == TRUE)	) {
						
						$this->$current_primary_key = $this->$name->save_to_database();
					}
				}				
			}	
			
			return TRUE;		
		}
		
		protected function save_list_to_database() {
		
			if ($this->_config_have_arraymember == FALSE) {
				return FALSE;
			}
			
			$table_object_array = array();			
			$this_var = get_object_vars($this);
	
			foreach ($this_var as $name => $value) {
				
				$array_type	= '_config_'.$name.'_array_type';
				$array_table	= '_config_'.$name.'_array_table_name';
				
				if (	(is_array($this->$name) == TRUE) &&
					
					(property_exists($this, $array_type) == TRUE) &&
					(empty($this->$array_type) == FALSE) &&
					(is_subclass_of(''.$this->$array_type.'', 'database_object') == TRUE) &&
					
					(property_exists($this, $array_table) == TRUE) &&
					(empty($this->$array_table) == FALSE) ) {
						
					if (empty($table_object_array[$this->$array_table]) == TRUE) {
						$table_object_array[$this->$array_table] = $name;
					} else {
						$table_object_array[$this->$array_table] .= ','.$name;
					}
				} 
			}

			if (empty($table_object_array) == TRUE) {
				return FALSE;
			}
			
			foreach ($table_object_array as $table_name => $object_list) {
				
				$object_array = explode(',', $object_list);				
				$foreign_key_field = '';
				eval('
					if (property_exists($this, "_config_'.$object_array[0].'_foreignkey_field") == TRUE) {
						$foreign_key_field = $this->'.'_config_'.$object_array[0].'_foreignkey_field;
					}
				');			
				
				$first_array_member = $object_array[0];				
				for ($array_count=0; $array_count < count($this->$first_array_member); $array_count++) {
					
					/*preparing for final field and value list for INSERT sql statement*/
					$current_field_list 		= '';
					$current_value_list 	= '';
					$current_field_array 	= array();
					$current_value_array 	= array();
					$final_field_value_array	= array();
					$final_field_list 	= '';
					$final_value_list 	= '';
					for ($object_count=0; $object_count < count($object_array); $object_count++) {
						
						/*setting foreign key to member objects*/
						$current_object_name				= $object_array[$object_count];
						$current_object_foreignkey_field 		= '_config_'.$current_object_name.'_foreignkey_field';
						$current_object_foreignkey_reference	= '_config_'.$current_object_name.'_foreignkey_reference';
						
						/*processing pre_save_to_database method*/
						eval ('
							if (method_exists($this->'.$current_object_name.'['.$array_count.'], pre_save_to_database) == TRUE) {
								$this->'.$current_object_name.'['.$array_count.']->pre_save_to_database();
							}
						');
						
						/*processing object member*/
						eval('
							$this->'.$current_object_name.'['.$array_count.']->save_objectmember_with_primarykey();
						');

						/*checking wether using REPLACE instead of INSERT or not*/
						eval('
							$current_primarykey_member = $this->'.$current_object_name.'['.$array_count.']->get_primarykey_member_name();
							$sql_insert = "";
							if (	($this->'.$current_object_name.'['.$array_count.']->can_replace_into() == TRUE) && 
									(empty($this->'.$current_object_name.'['.$array_count.']->$current_primarykey_member) == FALSE) &&
									($sql_insert == "")
								) {
								$sql_insert = "REPLACE";
							} else {
								$sql_insert = "INSERT";
							}
						');						
						
						if (	(property_exists($this, $current_object_foreignkey_field) == TRUE) &&
							(empty($this->$current_object_foreignkey_field) == FALSE) &&
							
							(property_exists($this, $current_object_foreignkey_reference) == TRUE) &&
							(empty($this->$current_object_foreignkey_reference) == FALSE) ) {
							
							eval('
								$current_foreignkey_member = $this->'.$current_object_name.'['.$array_count.']->get_associated_member($this->$current_object_foreignkey_field);
								if (empty($current_foreignkey_member) == FALSE) {
									$this->'.$current_object_name.'['.$array_count.']->$current_foreignkey_member = $this->'.$this->$current_object_foreignkey_reference.';
								}
							');								
						}						
						
						/*finding fields and values*/
						$first_array_member = $object_array[$object_count];
						eval('$this->'.$object_array[$object_count].'[$array_count]->get_fields_and_values_list($current_field_list, $current_value_list, $table_name, 1);');
						
						$current_field_list 	= substr($current_field_list, 0, -1);
						$current_value_list	= substr($current_value_list, 0, -1);
						
						$current_field_array 	= explode(',', $current_field_list);
						$current_value_array	= explode(',', $current_value_list);
						
						for ($current_array_count=(count($current_field_array) -1); $current_array_count >=0; $current_array_count--) {
							$final_field_value_array[$current_field_array[$current_array_count]] = $current_value_array[$current_array_count];
						}
					}
					foreach($final_field_value_array as $field => $value) {
						$final_field_list 	.= $field.',';
						$final_value_list	.= $value.',';
					}		
					$final_field_list 	= substr($final_field_list, 0, -1);
					$final_value_list	= substr($final_value_list, 0, -1);	
										
					/*building SQL statement*/
					$sql_header 	= $sql_insert.' INTO '.$table_name;
					$sql_fields	= ' ('.$final_field_list.')';
					$sql_values	= ' VALUES ('.$final_value_list.');';
					$sql = $sql_header . $sql_fields . $sql_values;
					
					/*connecting to database engine*/
					$db_instance = $this->connect_database($db_instance);
					
					/*die syntax in only development version*/
					$result = $db_instance->Execute($sql); //or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());
					
					/*preventing primary key member to be replaced by empty value if not using INSERT sql syntax*/
					eval ('
						if (empty($this->'.$current_object_name.'['.$array_count.']->$current_primarykey_member) == FALSE) {
							$current_primary_key =  $db_instance->Insert_ID();
						}
					');
					$db_instance->Close();
					
					/*setting primarykey to all objects*/
					for ($object_count=0; $object_count < count($object_array); $object_count++) {
					
						$current_object_name				= $object_array[$object_count];
						eval('
							$current_primarykey_member = $this->'.$current_object_name.'['.$array_count.']->get_primarykey_member_name();
							if (empty($current_primarykey_member) == FALSE) {
								$this->'.$current_object_name.'['.$array_count.']->$current_primarykey_member = $current_primary_key;
							}
						');
						
						/*Save data from array members to database*/
						eval('$this->'.$current_object_name.'['.$array_count.']->save_list_to_database();');
						
						/*saving to memory cache*/
						if (empty($current_primary_key) == FALSE) {
							if (is_subclass_of('database_object', 'memcached_object') == TRUE) {										
								eval('
									$table_offset 	= (empty($this->'.$current_object_name.'['.$array_count.']->_config_table_name) == FALSE ? $this->'.$current_object_name.'['.$array_count.']->_config_table_name : "");
									if ($this->'.$current_object_name.'['.$array_count.']->_config_have_arraymember == FALSE) {
										$this->'.$current_object_name.'['.$array_count.']->save_to_cache($table_offset.$current_primary_key);
									} else {
										$this->'.$current_object_name.'['.$array_count.']->delete_cache_in_array_keylist($table_offset.$current_primary_key);
									}
								');
							}				
						}						
					}					
				}

			}	
			
			return TRUE;		
		}
		
		function save_to_database() {
					
			/*Save data from members whom are objects associated with different table and with primary key defined to database*/
			$this->save_objectmember_with_primarykey();			
			
			/*saving normal member to database*/
			$primary_key_member = '';
			if (empty($this->_config_member_to_field) == FALSE) {

				$primary_key_member = $this->_config_primarykey_member;
				
				/*connecting to database engine*/
				$db_instance = $this->connect_database($db_instance);				
				
				/*checking wether using INSERT DELAYED or not*/
				$sql_insert_delayed = '';
				if (($this->_config_use_insert_delayed == 1) && ($this->can_insert_delayed() == TRUE)) {
					
					$sql = 'SHOW OPEN TABLES WHERE `Database`="'.DATABASE_NAME.'" AND `Table`="'.$this->_config_table_name.'" AND `In_use` != 0;';
					$data_array = $db_instance->GetAll($sql);
					
					if (count($data_array) > 0) {
						$sql_insert_delayed = 'DELAYED';
					}
				}
				
				$sql_insert = '';
				/*checking wether using REPLACE instead of INSERT or not*/
				if (($this->can_replace_into() == TRUE) && (empty($this->$primary_key_member) == FALSE)) {
					$sql_insert = 'REPLACE ';
				} else {
					$sql_insert = 'INSERT ';
				}

				$sql_header = $sql_insert.$sql_insert_delayed.' INTO '.$this->_config_table_name;
				$sql_fields = ' (';
				$sql_values = ' VALUES (';
				
				$this->get_fields_and_values_list($sql_fields, $sql_values, $this->_config_table_name, 1);
				
				$sql_fields 	= substr($sql_fields, 0, -1).')';
				$sql_values	= substr($sql_values, 0, -1).')';	
				$sql = $sql_header . $sql_fields . $sql_values . ';';
				
				/*die syntax in only development version*/
				$result = $db_instance->Execute($sql) or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());
				
				/*setting primarykey member equal to 0 if using insert delayed and not touching memory cache*/
				if ($sql_insert_delayed == 'DELAYED') {
					
					$this->$primary_key_member = 0;
					$db_instance->Close();
					
					return 0;
				} else {
					/*Preventing primarykey member to be replaced by empty value if not using INSERT sql syntax*/
					if (empty($this->$primary_key_member) == TRUE) {
						$this->$primary_key_member = $db_instance->Insert_ID();
					}
				}
				
				$db_instance->Close();	
			}			
			
			/*Save data from array members to database*/
			$this->save_list_to_database();
			
			/*saving to memory cache*/
			$final_primary_key = '';
			if (	(empty($primary_key_member) == FALSE) &&
			
				(property_exists($this, $primary_key_member) == TRUE) &&
				(empty($this->$primary_key_member) == FALSE) ) {
				
				$final_primary_key = $this->$primary_key_member;	
			}
			if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
				$table_offset 	= (empty($this->_config_table_name) == FALSE ? $this->_config_table_name.'.' : '');
				if ($this->_config_have_arraymember == FALSE) {
					/*$this->delete_cache($table_offset.$final_primary_key);*/
					$this->save_to_cache($table_offset.$final_primary_key);					
				} else {
					$this->delete_cache_in_array_keylist($table_offset.$final_primary_key);
				}
			}				

			return $final_primary_key;
		}
		
		protected function load_objectmember_with_primarykey() {
			
			if ($this->_config_have_objectmember == FALSE) {
				return FALSE;
			}
			
			$this_var = get_object_vars($this);
			foreach ($this_var as $name => $value) {
				$name_primary_key = '_config_'.$name.'_primary_key';
				
				if (	(is_object($this->$name) == TRUE) && 
					(is_subclass_of($this->$name, 'database_object') == TRUE) && 
		
					($this->$name->get_associated_table() != '') && 
					($this->$name->get_associated_table() != $this->_config_table_name) && 
					
					(property_exists($this, $name_primary_key) == TRUE) && 
					(empty($this->$name_primary_key) == FALSE)	) {
					
					$current_primary_key = $this->$name_primary_key;
					
					if (	(property_exists($this, $current_primary_key) == TRUE) && 
						(empty($this->$current_primary_key) == FALSE)	) {
					
						$this->$name->load_from_database($this->$current_primary_key);
					}
				}
			}
			
			return TRUE;		
		}
		
		function load_arraymember_from_database_sql($member_name='', $sql='', $offset=0, $limit=0) {
			
			if (empty($member_name) == TRUE) {
				return FALSE;
			}
			
			if (empty($sql) == TRUE) {
				return FALSE;
			}
			
			if (	(property_exists($this, $member_name) == FALSE) ||
				(is_array($this->$member_name) == FALSE)  ) {
				
				return FALSE;		
			}
			
			$array_table_name	= '_config_'.$member_name.'_array_table_name';
			if (property_exists($this, $array_table_name) == FALSE) {
				return FALSE;
			}
			
			$this->$member_name = array();
			
			$sql = trim($sql);
			$sql = preg_replace('/;$/', '', $sql);
			
			if ($this->_config_load_all_arraymember == 0) {
				if ($limit == 0) {
					$limit = $this->_config_max_arraymember_size;
				}
				$sql .= ' LIMIT '.$offset.','.$limit.';';
			} else {
				$offset = 0;
				$limit = 0;
				$sql .= ';';
			}
			
			
			/*loading from cache*/
			/*$key_sql = preg_replace('/[[:space:]]+/', '_', strtolower($sql));
			$current_primary_key_name = $this->_config_primarykey_member;
			if (	(empty($current_primary_key_name) == FALSE) &&
				(property_exists($this, $current_primary_key_name) == TRUE) &&
				(empty($this->$current_primary_key_name) == FALSE)) {
				
					$current_primary_key = $this->$current_primary_key_name;	
			}
			if (empty($current_primary_key) == FALSE) {
				if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
					$current_sql_offset = '.'.$key_sql;
					$current_table_offset 	= (empty($this->_config_table_name) == FALSE ? $this->_config_table_name.'.' : '');
					if ($this->load_from_cache($current_table_offset.$current_primary_key.".".$offset.":".$limit.$current_sql_offset) == TRUE) {
						return TRUE;
					}
				}
			}*/

			$db_instance = $this->connect_database($db_instance);	
			/*die syntax in only development version*/
			$data_array = $db_instance->GetAll($sql); // or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());
			$db_instance->Close();		
			
			$data_row_count=0;
			foreach ($data_array as $index => $data) {
			
				$array_prefix 			= '_config_'.$member_name.'_prefix';
				$array_firstpartreplace	= '_config_'.$member_name.'_first_part_replace';
				
				$prefix 			= '';
				$firstpartreplace 	= '';
				
				if (property_exists($this, $array_prefix) == TRUE) {
					$prefix = $this->$array_prefix;		
				} 
				if (property_exists($this, $array_firstpartreplace) == TRUE) {
					$firstpartreplace = $this->$array_firstpartreplace;
				}
				
				$array_type 	= '_config_'.$member_name.'_array_type';
				$object_type 	= $this->$array_type;						
				
				eval('$this->'.$member_name.'['.$data_row_count.'] = new '.$object_type.'("'.$this->$array_table_name.'", "'.$prefix.'", "'.$firstpartreplace.'");');
				eval('$this->'.$member_name.'['.$data_row_count.']->set_values_from_fields($data, "'.$this->$array_table_name.'");');

				/*Load data from members whom are objects associated with different table and with primary key defined*/ 
				eval('$this->'.$member_name.'['.$data_row_count.']->load_objectmember_with_primarykey();');
				
				/*Load data from member whom are array associated with foreign key with this object*/
				eval('$this->'.$member_name.'['.$data_row_count.']->load_list_from_database(0, $limit);');	
				
				/*saving to memory cache*/
				$current_object_name = $member_name;
				eval('
					$current_primary_key_name = $this->'.$current_object_name.'['.$data_row_count.']->get_primarykey_member_name();
					
					if (	(empty($current_primary_key_name) == FALSE) &&
						(property_exists($this->'.$current_object_name.'['.$data_row_count.'], $current_primary_key_name) == TRUE) &&
						(empty($this->'.$current_object_name.'['.$data_row_count.']->$current_primary_key_name) == FALSE)) {
							$current_primary_key = $this->'.$current_object_name.'['.$data_row_count.']->$current_primary_key_name;	
						}
				');					
				if (empty($current_primary_key) == FALSE) {
					if (is_subclass_of('database_object', 'memcached_object') == TRUE) {		
							
						eval('
							$current_sql_offset = "";
							$current_table_offset 	= (empty($this->'.$current_object_name.'['.$data_row_count.']->_config_table_name) == FALSE ? ($this->'.$current_object_name.'['.$data_row_count.']->_config_table_name.".") : "");				
							if ($this->'.$current_object_name.'['.$data_row_count.']->_config_have_arraymember == FALSE) {
								$this->'.$current_object_name.'['.$data_row_count.']->save_to_cache($current_table_offset.$current_primary_key);
							} else {
								//$current_sql_offset = $this->'.$current_object_name.'['.$data_row_count.']->get_cache_sql_offset();
								$this->'.$current_object_name.'['.$data_row_count.']->save_to_cache($current_table_offset.$current_primary_key.".".$offset.":".$limit.$current_sql_offset);
							}
						');
					}
				}							
				
				$data_row_count++;
			}
			
			/*saving to memory cache*/
			/*$current_primary_key_name = $this->_config_primarykey_member;
			if (	(empty($current_primary_key_name) == FALSE) &&
				(property_exists($this, $current_primary_key_name) == TRUE) &&
				(empty($this->$current_primary_key_name) == FALSE)) {
				
					$current_primary_key = $this->$current_primary_key_name;	
			}
			if (empty($current_primary_key) == FALSE) {
				if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
					$current_sql_offset = '.'.$key_sql;
					$current_table_offset 	= (empty($this->_config_table_name) == FALSE ? ($this->_config_table_name.'.') : '');
					$this->save_to_cache($current_table_offset.$current_primary_key.".".$offset.":".$limit.$current_sql_offset);
				}
			}*/
			
			return TRUE;
		}
		
		private function load_list_from_database_with_cache($select_offset=0, $select_limit=0) {
			
			$table_object_array = array();			
			$this_var = get_object_vars($this);

			if ($this->_config_load_all_arraymember == 0) {
				if ($select_limit == 0) {
					$select_limit = $this->_config_max_arraymember_size;
				}
			} else {
				$select_offset = 0;
				$select_limit = 0;
			}
			
			foreach ($this_var as $name => $value) {
				
				$array_type	= '_config_'.$name.'_array_type';
				$array_table	= '_config_'.$name.'_array_table_name';
				
				if (	(is_array($this->$name) == TRUE) &&
					
					(property_exists($this, $array_type) == TRUE) &&
					(empty($this->$array_type) == FALSE) &&
					(is_subclass_of(''.$this->$array_type.'', 'database_object') == TRUE) &&
					
					(property_exists($this, $array_table) == TRUE) &&
					(empty($this->$array_table) == FALSE) ) {
						
					if (empty($table_object_array[$this->$array_table]) == TRUE) {
						$table_object_array[$this->$array_table] = $name;
					} else {
						$table_object_array[$this->$array_table] .= ','.$name;
					}
					
				} 
			}
			
			if (empty($table_object_array) == TRUE) {
				return FALSE;
			}
			
			foreach ($table_object_array as $name => $value) {
				
				$object_array = explode(',', $value);
				
				$foreign_key_field = '';
				eval('
					if (property_exists($this, "_config_'.$object_array[0].'_foreignkey_field") == TRUE) {
						$foreign_key_field = $this->'.'_config_'.$object_array[0].'_foreignkey_field;
					}
				');				
				
				$field_list = '';
				$final_where_hook = '';
				$final_group_hook = '';
				$final_order_hook = '';
				$count =0;				
				
				/*building sql where and order hook*/
				foreach ($object_array as $object_index => $object_name) {
				
					$array_prefix 			= '_config_'.$object_name.'_prefix';
					$array_firstpartreplace	= '_config_'.$object_name.'_first_part_replace';
					
					$prefix 			= '';
					$firstpartreplace 	= '';
					
					if (property_exists($this, $array_prefix) == TRUE) {
						$prefix = $this->$array_prefix;		
					} 
					if (property_exists($this, $array_firstpartreplace) == TRUE) {
						$firstpartreplace = $this->$array_firstpartreplace;
					}
					
					$array_type 	= '_config_'.$object_name.'_array_type';
					$object_type 	= $this->$array_type;
					
					eval('
						$this->'.$object_name.'[0] = new '.$object_type.'("'.$name.'", "'.$prefix.'", "'.$firstpartreplace.'");
						$current_primarykey_member = $this->'.$object_name.'[0]->get_primarykey_member_name();
						$current_primarykey_field =  $this->'.$object_name.'[0]->get_associated_field($current_primarykey_member);
					');
					
					/*check for duplicate field name in list*/
					if (preg_match('/'.$current_primarykey_field.',/', $field_list) == FALSE) {
						$field_list .= $current_primarykey_field.',';
					} 
				
					/*processing SQL WHERE hook*/
					$sql_where_hook 	= '_config_'.$object_name.'_sql_where_hook';
					if (	(property_exists($this, $sql_where_hook) == TRUE) &&
						(empty($this->$sql_where_hook) == FALSE) ) {

						eval('$final_where_hook .= " ('.$this->$sql_where_hook.') AND";');		
					}

					/*processing SQL GROUP hook*/
					$sql_group_hook		= '_config_'.$object_name.'_sql_group_hook';
					if (	(property_exists($this, $sql_group_hook) == TRUE) &&
						(empty($this->$sql_group_hook) == FALSE) ) {

						eval('$final_group_hook .= " '.$this->$sql_group_hook.',";');		
					}					
					
					/*processing SQL ORDER hook*/
					$sql_order_hook	= '_config_'.$object_name.'_sql_order_hook';
					if (	(property_exists($this, $sql_order_hook) == TRUE) &&
						(empty($this->$sql_order_hook) == FALSE) ) {
												
						eval('$final_order_hook .= " '.$this->$sql_order_hook.',";');		
					}						
				}

				$sql_header 	= 'SELECT ';
				$sql_fields 	= substr($field_list, 0, -1).' FROM '.$name;	
				$sql_where 	= '';
				
				if (empty($foreign_key_field) == FALSE) {
					
					eval('
						if (property_exists($this, "'.'_config_'.$object_array[0].'_foreignkey_reference") == FALSE) {
							$foreign_key_reference = $this->_config_primarykey_member;
						}  else {
							$foreign_key_reference = $this->'.'_config_'.$object_array[0].'_foreignkey_reference;
						}
					');
					
					if (	(property_exists($this, $foreign_key_reference) == FALSE) ||
						(empty($this->$foreign_key_reference) == TRUE) ) {
						
						$foreign_key_reference = $this->_config_primarykey_member;							
					} 
					
					$sql_where 	= ' WHERE '.$foreign_key_field.' = '.$this->fill_sql_quote($foreign_key_reference);
				} 
				
				/*finishing SQL WHERE statement*/
				if ($sql_where == '') {
					if (empty($final_where_hook) == FALSE) {
						$final_where_hook = substr($final_where_hook, 0, -3);
						$sql_where = 'WHERE '.$final_where_hook;
					}
				} else {
					if (empty($final_where_hook) == FALSE) {
						$final_where_hook = substr($final_where_hook, 0, -3);
						$sql_where .= ' AND '.$final_where_hook;
					}
				}

				/*building SQL GROUP statement*/
				$sql_group = '';
				if (empty($final_group_hook) == FALSE) {
					$sql_group = ' GROUP BY '.substr($final_group_hook, 0, -1);
				}
				
				/*building SQL ORDER statement*/
				$sql_order = '';
				if (empty($final_order_hook) == FALSE) {
					$sql_order = ' ORDER BY '.substr($final_order_hook, 0, -1);
				}			
				
				$sql_limit = ';';
				if ($this->_config_load_all_arraymember == 0) {
					$sql_limit 	= ' LIMIT '.$select_offset.', '.$select_limit.';';
				}
				
				$sql = $sql_header . $sql_fields . $sql_where. $sql_group. $sql_order . $sql_limit;
				
				$db_instance = $this->connect_database($db_instance);	
				/*die syntax in only development version*/
				$data_array = $db_instance->GetAll($sql); // or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());
				$db_instance->Close();
				
				/*No object needed, kill all created objects*/
				if (($data_array === FALSE) || (is_array($data_array) == FALSE) || (count($data_array) == 0)) {
					foreach ($object_array as $object_index => $object_name) {
						eval('unset($this->'.$object_name.'[0]);');						
					}			
				} else {
					/*using primarykey to load data from database*/
					foreach ($object_array as $object_index => $object_name) {		
					
						/*finding primarykey field*/
						eval('
							$this->'.$object_name.'[0] = new '.$object_type.'("'.$name.'", "'.$prefix.'", "'.$firstpartreplace.'");
							$current_primarykey_member = $this->'.$object_name.'[0]->get_primarykey_member_name();
							$current_primarykey_field =  $this->'.$object_name.'[0]->get_associated_field($current_primarykey_member);
						');				
						
						foreach ($data_array as $row_index => $row_data) {
									
							if ($row_index > 0) {
								/*new object to array member*/
								$array_prefix 			= '_config_'.$object_name.'_prefix';
								$array_firstpartreplace	= '_config_'.$object_name.'_first_part_replace';
								
								$prefix 			= '';
								$firstpartreplace 	= '';
								
								if (property_exists($this, $array_prefix) == TRUE) {
									$prefix = $this->$array_prefix;		
								} 
								if (property_exists($this, $array_firstpartreplace) == TRUE) {
									$firstpartreplace = $this->$array_firstpartreplace;
								}
								
								$array_type 	= '_config_'.$object_name.'_array_type';
								$object_type 	= $this->$array_type;						
								
								eval('$this->'.$object_name.'['.$row_index.'] = new '.$object_type.'("'.$name.'", "'.$prefix.'", "'.$firstpartreplace.'");');																	
							}		
							/*load data from database*/
							eval('
								$this->'.$object_name.'['.$row_index.']->load_from_database($row_data[$current_primarykey_field], 0, $select_limit);
							');								
						}
					}					
				}
			}		
			
			return TRUE;		
		}
		private function load_list_from_database_without_cache($select_offset=0, $select_limit=0) {

			$table_object_array = array();			
			$this_var = get_object_vars($this);
			
			if ($this->_config_load_all_arraymember == 0) {
				if ($select_limit == 0) {
					$select_limit = $this->_config_max_arraymember_size;
				}
			} else {
				$select_offset = 0;
				$select_limit = 0;
			}			
	
			foreach ($this_var as $name => $value) {
				
				$array_type	= '_config_'.$name.'_array_type';
				$array_table	= '_config_'.$name.'_array_table_name';
				
				if (	(is_array($this->$name) == TRUE) &&
					
					(property_exists($this, $array_type) == TRUE) &&
					(empty($this->$array_type) == FALSE) &&
					(is_subclass_of(''.$this->$array_type.'', 'database_object') == TRUE) &&
					
					(property_exists($this, $array_table) == TRUE) &&
					(empty($this->$array_table) == FALSE) ) {
						
					if (empty($table_object_array[$this->$array_table]) == TRUE) {
						$table_object_array[$this->$array_table] = $name;
					} else {
						$table_object_array[$this->$array_table] .= ','.$name;
					}
				} 
			}
			
			if (empty($table_object_array) == TRUE) {
				return FALSE;
			}
			
			foreach ($table_object_array as $name => $value) {
				
				$object_array = explode(',', $value);
				
				$foreign_key_field = '';
				eval('
					if (property_exists($this, "_config_'.$object_array[0].'_foreignkey_field") == TRUE) {
						$foreign_key_field = $this->'.'_config_'.$object_array[0].'_foreignkey_field;
					}
				');
				
				$field_list = '';
				$final_where_hook = '';
				$final_group_hook = '';
				$final_order_hook = '';
				$count =0;
				for ($count=0; $count < count($object_array); $count++) {
					
					$array_prefix 			= '_config_'.$object_array[$count].'_prefix';
					$array_firstpartreplace	= '_config_'.$object_array[$count].'_first_part_replace';
					
					$prefix 			= '';
					$firstpartreplace 	= '';
					
					if (property_exists($this, $array_prefix) == TRUE) {
						$prefix = $this->$array_prefix;		
					} 
					if (property_exists($this, $array_firstpartreplace) == TRUE) {
						$firstpartreplace = $this->$array_firstpartreplace;
					}
					
					$array_type 	= '_config_'.$object_array[$count].'_array_type';
					$object_type 	= $this->$array_type;
					
					eval('$this->'.$object_array[$count].'[0] = new '.$object_type.'("'.$name.'", "'.$prefix.'", "'.$firstpartreplace.'");');
					eval('$this->'.$object_array[$count].'[0]->get_fields_and_values_list($field_list, $value_list_tmp, $name, 0);');
					
					/*processing SQL WHERE hook*/
					$sql_where_hook 	= '_config_'.$object_array[$count].'_sql_where_hook';
					if (	(property_exists($this, $sql_where_hook) == TRUE) &&
						(empty($this->$sql_where_hook) == FALSE) ) {

						eval('$final_where_hook .= " ('.$this->$sql_where_hook.') AND";');		
					}

					/*processing SQL GROUP hook*/
					$sql_group_hook		= '_config_'.$object_array[$count].'_sql_group_hook';
					if (	(property_exists($this, $sql_group_hook) == TRUE) &&
						(empty($this->$sql_group_hook) == FALSE) ) {

						eval('$final_group_hook .= " '.$this->$sql_group_hook.',";');		
					}						
					
					/*processing SQL ORDER hook*/
					$sql_order_hook	= '_config_'.$object_array[$count].'_sql_order_hook';
					if (	(property_exists($this, $sql_order_hook) == TRUE) &&
						(empty($this->$sql_order_hook) == FALSE) ) {
												
						eval('$final_order_hook .= " '.$this->$sql_order_hook.',";');		
					}					
				}
				
				$sql_header 	= 'SELECT ';
				$sql_fields 	= substr($field_list, 0, -1).' FROM '.$name;	
				$sql_where 	= '';
				
				if (empty($foreign_key_field) == FALSE) {
					
					eval('
						if (property_exists($this, "'.'_config_'.$object_array[0].'_foreignkey_reference") == FALSE) {
							$foreign_key_reference = $this->_config_primarykey_member;
						}  else {
							$foreign_key_reference = $this->'.'_config_'.$object_array[0].'_foreignkey_reference;
						}
					');
					
					if (	(property_exists($this, $foreign_key_reference) == FALSE) ||
						(empty($this->$foreign_key_reference) == TRUE) ) {
						
						$foreign_key_reference = $this->_config_primarykey_member;							
					} 
					
					$sql_where 	= ' WHERE '.$foreign_key_field.' = '.$this->fill_sql_quote($foreign_key_reference);
				} 
				
				/*finishing SQL WHERE statement*/
				if ($sql_where == '') {
					if (empty($final_where_hook) == FALSE) {
						$final_where_hook = substr($final_where_hook, 0, -3);
						$sql_where = 'WHERE '.$final_where_hook;
					}
				} else {
					if (empty($final_where_hook) == FALSE) {
						$final_where_hook = substr($final_where_hook, 0, -3);
						$sql_where .= ' AND '.$final_where_hook;
					}
				}

				/*building SQL GROUP statement*/
				$sql_group = '';
				if (empty($final_group_hook) == FALSE) {
					$sql_group = ' GROUP BY '.substr($final_group_hook, 0, -1);
				}	

				/*building SQL ORDER statement*/
				$sql_order = '';
				if (empty($final_order_hook) == FALSE) {
					$sql_order = ' ORDER BY '.substr($final_order_hook, 0, -1);
				}			
				
				$sql_limit = ';';
				if ($this->_config_load_all_arraymember == 0) {
					$sql_limit 	= ' LIMIT '.$select_offset.', '.$select_limit.';';
				}				
				$sql = $sql_header . $sql_fields . $sql_where . $sql_group . $sql_order . $sql_limit;
				
				$db_instance = $this->connect_database($db_instance);	
				/*die syntax in only development version*/
				$data_array = $db_instance->GetAll($sql); // or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());
				$db_instance->Close();
				
				/*No object needed, kill all created objects*/
				if (count($data_array) == 0) {
					$count =0;
					for ($count=0; $count < count($object_array); $count++) {
						eval('unset($this->'.$object_array[$count].'[0]);');						
					}			
				}
				
				/*Fill object array with loaded data*/
				$data_row_count =0;
				for ($data_row_count=0; $data_row_count < count($data_array); $data_row_count++) {	
					
					for ($object_count=0; $object_count < count($object_array); $object_count++) {
						
						if ($data_row_count > 0) {	
						
							$array_prefix 			= '_config_'.$object_array[$object_count].'_prefix';
							$array_firstpartreplace	= '_config_'.$object_array[$object_count].'_first_part_replace';
							
							$prefix 			= '';
							$firstpartreplace 	= '';
							
							if (property_exists($this, $array_prefix) == TRUE) {
								$prefix = $this->$array_prefix;		
							} 
							if (property_exists($this, $array_firstpartreplace) == TRUE) {
								$firstpartreplace = $this->$array_firstpartreplace;
							}
							
							$array_type 	= '_config_'.$object_array[$object_count].'_array_type';
							$object_type 	= $this->$array_type;						
							
							eval('$this->'.$object_array[$object_count].'['.$data_row_count.'] = new '.$object_type.'("'.$name.'", "'.$prefix.'", "'.$firstpartreplace.'");');			
						}			
						
						eval('$this->'.$object_array[$object_count].'['.$data_row_count.']->set_values_from_fields($data_array['.$data_row_count.'], $name);');
																	
						/*Load data from members whom are objects associated with different table and with primary key defined*/ 
						eval('$this->'.$object_array[$object_count].'['.$data_row_count.']->load_objectmember_with_primarykey();');
						
						/*Load data from member whom are array associated with foreign key with this object*/
						eval('$this->'.$object_array[$object_count].'['.$data_row_count.']->load_list_from_database();');
			
					}				
				}
			}

			return TRUE;			
		}
		
		protected function load_list_from_database($select_offset=0, $select_limit=0) {
		
			if ($this->_config_have_arraymember == FALSE) {
				return FALSE;
			}		
		
			/*select wether under caching environment or not*/
			if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
				/*load data under caching system environment*/
				return $this->load_list_from_database_with_cache($select_offset, $select_limit);
			} else {
				/*load data under direct database engine environment*/
				return $this->load_list_from_database_without_cache($select_offset, $select_limit);
			}
		}
		
		function load_from_database($primary_key_value='', $select_offset=0, $select_limit=0) {
			
			if ($this->_config_load_all_arraymember == 0) {
				if ($select_limit == 0) {
					$select_limit = $this->_config_max_arraymember_size;
				}
			} else {
				$select_offset = 0;
				$select_limit = 0;
			}
			
			/*loading from memory cache*/
			if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
				$sql_offset = '';
				$table_offset 	= (empty($this->_config_table_name) == FALSE ? $this->_config_table_name.'.' : '');
				if ($this->_config_have_arraymember == TRUE) {
					$sql_offset = $this->get_cache_sql_offset();
					if ($this->load_from_cache($table_offset.$primary_key_value.'.'.$select_offset.':'.$select_limit.$sql_offset) == TRUE) {
						return TRUE;
					}
				} else {
					if ($this->load_from_cache($table_offset.$primary_key_value) == TRUE) {
						return TRUE;
					}					
				}
			}		
			
			if (empty($this->_config_member_to_field) == TRUE) {
			
				if (empty($primary_key_value) == FALSE) {
					return FALSE;
				}				
				
				return $this->load_list_from_database($select_offset, $select_limit);
			}
			
			if (is_numeric($select_offset) == FALSE) {
				return FALSE;
			}
			
			if (is_numeric($select_limit) == FALSE) {
				return FALSE;
			}
			
			if (empty($primary_key_value) == TRUE) {
				return FALSE;
			}
			
			$primary_key_member = $this->_config_primarykey_member;
			$this->$primary_key_member = $primary_key_value;		
			
			$sql_header 	= 'SELECT ';
			$sql_fields 	= '';
			$sql_where 	= ' WHERE '.$this->_config_member_to_field[$primary_key_member].' = '.$this->fill_sql_quote($primary_key_member);
			
			$this->get_fields_and_values_list($sql_fields, $value_temp, $this->_config_table_name, 0);
			
			$sql_fields 	= substr($sql_fields, 0, -1).' FROM '.$this->_config_table_name;	
			$sql = $sql_header . $sql_fields . $sql_where;

			$db_instance = $this->connect_database($db_instance);	
			/*die syntax in only development version*/
			$row_array = $db_instance->GetRow($sql); //or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());		
			
			$this->set_values_from_fields($row_array, $this->_config_table_name);
			
			$db_instance->Close();
			
			/*Load data from members whom are objects associated with different table and with primary key defined*/ 
			$this->load_objectmember_with_primarykey();
			
			/*Load data from member whom are array associated with foreign key with this object*/
			$this->load_list_from_database($select_offset, $select_limit);
			
			/*saving to memory cache*/
			if (is_subclass_of('database_object', 'memcached_object') == TRUE) {				
				$sql_offset = '';
				$table_offset 	= (empty($this->_config_table_name) == FALSE ? $this->_config_table_name.'.' : '');
				if ($this->_config_have_arraymember == TRUE) {
					$sql_offset = $this->get_cache_sql_offset();
					$this->save_to_cache($table_offset.$primary_key_value.'.'.$select_offset.':'.$select_limit.$sql_offset);
				} else {
					$this->save_to_cache($table_offset.$primary_key_value);
				}
			}
			
			return TRUE;
		}
		
		protected function get_filled_member_list() {

			$filled_member_list = '';	
			
			/*Buiding part of SQL statement form filled member for database updating*/
			/*Primary Key Member is not included*/
			foreach ($this->_config_member_to_field as $name => $value) {	
				if ((empty($this->$name) == false) && ($this->_config_primarykey_member != $name)) {
					$filled_member_list .= $value.'='.$this->fill_sql_quote($name).',';
				}
			}			
			$filled_member_list = substr($filled_member_list, 0, -1);
			
			return $filled_member_list;
		}
		
		function find_in_database($sql_order='', $unequal_condition_list=array(), $find_offset=0, $select_offset=0, $select_limit=0) {
		
			$this_vars = get_object_vars($this);
			$sql_where = '';
			
			$primarykey_member = $this->_config_primarykey_member;
			if ($primarykey_member == '') {
				return FALSE;
			}
			
			//if primary key is provided
			if ((is_numeric($this->$primarykey_member) == TRUE) && ($this->$primarykey_member > 0)) {
				return $this->load_from_database($this->$primarykey_member);
			}
			
			$this->$primarykey_member = '';
			
			if ((is_numeric($find_offset) == FALSE) || ($find_offset < 0)) {
				$find_offset = 0;
			}
			
			/*protecting not-array error*/
			if (is_array($unequal_condition_list) == FALSE) {
				$unequal_condition_list = array();
			}
			
			/*building sql WHERE conditions*/
			foreach ($this_vars as $name => $value) {
				
				if (empty($unequal_condition_list[$name]) == TRUE) {
					$comparator = '=';
				} else {
					$comparator =  $unequal_condition_list[$name];
				}
				
				if (($condtion = $this->get_sql_condition($name, $comparator)) != '') {
					$sql_where .= ' AND ' . $condtion;
				}
			}
			
			/*building sql ORDER BY if not defined*/
			if ($sql_order == '') {
				$sql_order = $this->get_associated_field($this->get_primarykey_member_name()) . ' ASC';
			}
			
			/*building full sql query*/
			$sql = '
				SELECT '.$this->get_associated_field($this->get_primarykey_member_name()).' FROM '.$this->get_associated_table().'
				WHERE 1
				'.$sql_where.'
				ORDER BY '.$sql_order.'
				LIMIT '.$find_offset.', 1;
			';
			
			$db_instance = $this->connect_database($db_instance);
			/*die syntax in only development version*/
			$data_array = $db_instance->GetAll($sql); // or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());
			$db_instance->Close();
				
			/*nothing found*/
			if (count($data_array) == 0) {
				return FALSE;
			}
							
			$result = $this->load_from_database($data_array[0][0], $select_offset, $select_limit);
			
			return $result;
		}
		
		function find_primarykey_list_in_database($sql_order='', $unequal_condition_list=array(), $select_offset=0, $select_limit=0) {

			$this_vars = get_object_vars($this);
			$sql_where = '';
			
			$primarykey_member = $this->_config_primarykey_member;
			if ($primarykey_member == '') {
				return FALSE;
			}
			
			//if primary key is provided
			if ((is_numeric($this->$primarykey_member) == TRUE) && ($this->$primarykey_member > 0)) {
				return array(0 =>$this->$primarykey_member);
			}
						
			$this->$primarykey_member = '';			
				
			/*protecting not-array error*/
			if (is_array($unequal_condition_list) == FALSE) {
				$unequal_condition_list = array();
			}
				
			/*building sql WHERE conditions*/
			foreach ($this_vars as $name => $value) {
				
				if (empty($unequal_condition_list[$name]) == TRUE) {
					$comparator = '=';
				} else {
					$comparator =  $unequal_condition_list[$name];
				}
				
				if (($condtion = $this->get_sql_condition($name, $comparator)) != '') {
					$sql_where .= ' AND ' . $condtion;
				}
			}
				
			/*building sql ORDER BY if not defined*/
			if ($sql_order == '') {
				$sql_order = $this->get_associated_field($this->get_primarykey_member_name()) . ' ASC';
			}
			
			/*building sql LIMIT*/
			if ($select_limit == 0) {
				$sql_limit = ';';
			} else {
				$sql_limit = '
					LIMIT '.$select_offset.', '.$select_limit.';
				';
			}
			
			/*building full sql query*/
			$sql = '
				SELECT '.$this->get_associated_field($this->get_primarykey_member_name()).' FROM '.$this->get_associated_table().'
				WHERE 1
				'.$sql_where.'
				ORDER BY '.$sql_order.'
				'.$sql_limit.'
			';
			
			$db_instance = $this->connect_database($db_instance);
			/*die syntax in only development version*/
			$data_array = $db_instance->GetAll($sql); // or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());
			$db_instance->Close();

			/*nothing found*/
			if (count($data_array) == 0) {
				return FALSE;
			}
			
			$result_array = array();
			foreach ($data_array as $row_index => $row_array) {
				$result_array[] = $row_array[0];
			}
			
			return $result_array;
		}
		
		function delete_object_cache($select_offset=0, $select_limit=0) {
			
			$primarykey_member = $this->_config_primarykey_member;
			if (	($primarykey_member == '') ||
					($this->$primarykey_member == '')	) {
				return FALSE;
			}
			
			if ($this->_config_load_all_arraymember == 0) {
				if ($select_limit == 0) {
					$select_limit = $this->_config_max_arraymember_size;
				}
			} else {
				$select_offset = 0;
				$select_limit = 0;
			}			
			
			/*deleting memory cache*/
			if (is_subclass_of('database_object', 'memcached_object') == TRUE) {
				$sql_offset = '';
				$table_offset 	= (empty($this->_config_table_name) == FALSE ? $this->_config_table_name.'.' : '');
				if ($this->_config_have_arraymember == TRUE) {
					$sql_offset = $this->get_cache_sql_offset();
					$this->delete_cache($table_offset.$this->$primarykey_member.'.'.$select_offset.':'.$select_limit.$sql_offset);
				} else {
					$this->delete_cache($table_offset.$this->$primarykey_member);
				}
			} else {
				return FALSE;
			}
			return TRUE;
		}
		
		/*delete only this object not delete member object from database*/
		function delete_object_from_database() {

			$primarykey_member = $this->_config_primarykey_member;

			if (	($primarykey_member == '') ||
					($this->$primarykey_member == '')	) {
				return FALSE;
			}	
			
			$this->delete_object_cache();

			$sql_header 	= 'DELETE FROM '.$this->_config_table_name;			
			$sql_where 	= ' WHERE '.$this->_config_member_to_field[$primarykey_member].' = '.$this->fill_sql_quote($primarykey_member);
			$sql = $sql_header . $sql_where;
			
			$db_instance = $this->connect_database($db_instance);
			/*die syntax in only development version*/
			$db_instance->Execute($sql); //or die ('Error in sql: '. $sql .' '. $db_instance->ErrorMsg());
			$db_instance->Close();
			
			return TRUE;
		}
		
	}	
	
?>