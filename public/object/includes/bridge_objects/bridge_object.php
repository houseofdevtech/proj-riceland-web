<?php
/**
 * Bridge for Transfering data between server node
 * Created on 28-1-2014 by Yim
 */

	$script_path = dirname(__FILE__).'/';

	require_once($script_path.'../libs_path.php');
	require_once(RSF_STANDARD_OBJECTS_PATH);
	require_once(ANTI_MCRYPT);

	class bridge_object {
		
		private $_config_input_object_type;
		protected $_config_destination_url;
		
		private $input_object;
		private $command_list;
		
		function bridge_object(standard_object &$input_object = NULL) {
			
			if ($input_object == NULL) {
				throw new Exception('Invalid input_object for bridge_object');
				exit;
			}
			
			if (is_subclass_of($input_object, 'standard_object') == false) {
				throw new Exception('Invalid input_object for bridge_object');
				exit;
			}
			
			$this->_config_input_object_type = get_class($input_object);
			$this->input_object = $input_object;
			
			$this->command_list = array();
		}
		
		function set_destination_url($destination_url) {
			$this->_config_destination_url = $destination_url;
		}
		
		function add_command_list($command) {
			$this->command_list[count($this->command_list)] = $command;
		}
		
		function clear_command_list($command) {
			$this->command_list = array();
		}
		
		function execute_command($command) {
			
			if (empty($command) == true) {
				return('ERROR_COMMAND_EMPTY');
			}
			
			if (empty($this->_config_destination_url) == true) {
				return('ERROR_DESTURL_EMPTY');
			}
			
			$mcrypt_obj = new Anti_Mcrypt(basename($this->_config_destination_url));
			$msg_object = new stdClass();
			
			$msg_object->object = serialize($this->input_object);
			$msg_object->command[0] = $command;
			
			$encrypted_msg = $mcrypt_obj->encrypt(serialize($msg_object));
			
			$curl_ojbect = curl_init();
			curl_setopt($curl_ojbect, CURLOPT_URL, $this->_config_destination_url);
			curl_setopt($curl_ojbect, CURLOPT_POST, 1);
			curl_setopt($curl_ojbect, CURLOPT_POSTFIELDS, http_build_query(array('bridge_msg' => $encrypted_msg)));
			curl_setopt($curl_ojbect, CURLOPT_RETURNTRANSFER, true);
			
			$response = curl_exec($curl_ojbect);
			
			curl_close($curl_ojbect);
			unset($encrypted_msg);
			unset($msg_object);
			
			if (empty($response) == true) {
				return('ERROR_RESPONSE_EMPTY');
			}
			 
			$decrpyted_response = $mcrypt_obj->decrypt($response);
			
			if (empty($decrpyted_response) == true) {
				return('ERROR_DECRYPT_ERROR');
			}
			
			$response_msg_object = unserialize($decrpyted_response);
			
			if (empty($response_msg_object) == true) {				
				return($decrpyted_response.' ERROR_EMPTY_RESPONSE_MSG_OBJECT');
			}
			unset($decrpyted_response);
			
			$response_object = unserialize($response_msg_object->object);
			if (empty($response_object) == true) {
				return('ERROR_EMPTY_RESPONSE_OBJECT');
			}
			if (empty($response_msg_object->result) == true) {
				return('ERROR_EMPTY_RESPONSE_RESULT');
			}
			$response_result = $response_msg_object->result;
			
			unset($response_msg_object);
			
			if (get_class($response_object) != get_class($this->input_object)) {
				return('ERROR_MISSTYPE_RESPONSE_OBJECT');
			}
			
			$this->input_object->set_from_object($response_object);
			unset($response_object);
			
			return $response_result;
		}
	}

?>