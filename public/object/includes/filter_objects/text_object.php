<?php
/**
*	Common Text Filter Handling Object
* 	Created 14-7-2012
* */	

	class text_object {
		
		private $_config_text_charset;
		protected $_data_original_text;
		protected $_data_processing_text;
		
		function text_object($text, $charset='utf-8') {
			$this->_config_text_charset = $charset;
			$this->_data_original_text = $text;
			
			$this->reset_to_originaltext();
		}
		
		function reset_to_originaltext() {
			$this->_data_processing_text = $this->_data_original_text;
		}
		
		function set_text($text) {
			$this->_data_original_text = $text;
			$this->reset_to_originaltext();
		}
		
		function set_charset($charset) {
			$this->_config_text_charset = $charset;
		}
		
		function get_process_text() {
			return $this->_data_processing_text;
		}
		
		function get_original_text() {
			return $this->_data_original_text;
		}
		
		function limit_text($length='100', $replacer='...') {
			if (empty($this->_data_processing_text) == TRUE) {
				return FALSE;
			}
			
			if ((mb_strlen($this->_data_processing_text, $this->_config_text_charset) + strlen($replacer)) <= $length) {
				return $this->_data_processing_text;
			}
			
			$this->_data_processing_text = mb_substr($this->_data_processing_text, 0, $length, $this->_config_text_charset).$replacer;
			return $this->_data_processing_text;
		}
	}
?>