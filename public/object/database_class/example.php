<?php

	$script_path = dirname(__FILE__).'/';

	require_once($script_path.'../object_path.php');
	require_once(RSF_DATABASE_OBJECTS_PATH_WITHOUT_MEMCACHE);

	class user extends database_object {


		var
			$user_id,
			$name,
			$user_token,
			$user_img_url,
			$phone,
			$last_active_battery_percent;

		private 
			$table_name;


		function __construct($table_name = 'user'){

			$this->table_name = $table_name;
			parent::database_object($table_name);

			$this->user_img_url = 'https://cufindme.com/client_scripts/images/user_img/default.png';
			$this->phone = '0851100255';
		}

		function get_id_from_user_token($user_token){

			$sql = "
				SELECT user_id 
				FROM ".$this->table_name." 
				WHERE user_token = '".$user_token."';
			";
			$db_instance = database_object::connect_database($db_instance);
		    $result = $db_instance->GetAll($sql);
		    $user_id = "-1";
		    if($result != NULL){
		    	$user_id = $result[0][0];
		    }
		    else{
		    	$this->create_new_user_from_token($user_token);
		    	$user_id = $this->user_id;
		    }
		    return $user_id; 
		}

		private function create_new_user_from_token($user_token){
			$this->name = "New Token ".date('Y-m-d G:i:s');;
			$this->user_token = $user_token;
			$this->user_img_url = 'https://cufindme.com/client_scripts/images/user_img/default.png';
			$this->phone = '-';

			$this->user_id = $this->save_to_database();
		}

		function get_all_user_id(){
			$sql = "
				SELECT user_id 
				FROM ".$this->table_name.";
			";
			$db_instance = database_object::connect_database($db_instance);
		    $result = $db_instance->GetAll($sql);

		    return $result; 
		}

		

	}

?>