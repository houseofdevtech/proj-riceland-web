<?php

    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    $script_path = dirname(__FILE__);
    require_once($script_path.'/object/object_path.php');
    require_once(SMARTY_PATH3);
    require_once(EXAMPLE);
    $allproduct = require_once('./utils/get_product.php');

	$smarty_instance = new Smarty;	
    $smarty_instance->caching = false;	

    // Import Content
    $menu = require_once('./content/menu.php');
    $company_profile = require_once('./content/company_profile.php');
    $our_mission = require_once('./content/our_mission.php');
    $how_to_order = require_once('./content/how_to_order.php');
    $contact_us = require_once('./content/contact_us.php');
    
    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    }
    else{
        $lang = null;
    }
    
    if($lang === 'en' || $lang === null){
        $menu = $menu['en'];
        $content = [
            'welcome'=>'WELCOME TO',
            'riceland'=>'RICELAND FOODS LTD.',
            'company_profile'=>$company_profile['en'],
            'our_vision'=>[
                'title'=>'OUR VISION',
                'p1'=>'Be your best partner in the High-end rice field'
            ],
            'our_mission'=>$our_mission['en'],
            'our_products'=>[
                'title'=>'OUR PRODUCTS',
                'specification'=>'Specification of',
                'description'=>'Description',
                'standard'=>'Thai Rice standard'
            ],
            'certificates'=>[
                'title'=>'CERTIFICATES'
            ],
            'form'=>[
                'name'=>'Your Name*',
                'email'=>'E-mail*',
                'tel'=>'Telephone Number*',
                'remark'=>'Remark',
                'submit'=> 'Submit'
            ],
            'how_to_order'=>$how_to_order['en'],
            'contact_us'=>$contact_us['en']
        ];
        // $our_products = [
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //             // [
        //             //     'description'=>'Moisture',
        //             //     'standard'=>'14% max'
        //             // ],
        //             // [
        //             //     'description'=>'Whole Kernels',
        //             //     'standard'=>'60% min'
        //             // ],
        //             // [
        //             //     'description'=>'Broken (5.2 mm.)',
        //             //     'standard'=>'4.5% max'
        //             // ],
        //             // [
        //             //     'description'=>'Redkernels',
        //             //     'standard'=>'0%'
        //             // ],
        //             // [
        //             //     'description'=>'Yellow kernels',
        //             //     'standard'=>'0.2% max'
        //             // ],
        //             // [
        //             //     'description'=>'Chalky kernels',
        //             //     'standard'=>'6% max'
        //             // ],
        //             // [
        //             //     'description'=>'Damaged kernels',
        //             //     'standard'=>'0.25% max'
        //             // ],
        //             // [
        //             //     'description'=>'White Glutinous Rice',
        //             //     'standard'=>'1.5% max'
        //             // ],
        //             // [
        //             //     'description'=>'Undeveloped, Immature kernels, Other seeds and Foreign Matter',
        //             //     'standard'=>'0.2% max'
        //             // ],
        //             // [
        //             //     'description'=>'Paddy (grain per 1 kg.)',
        //             //     'standard'=>'7 grains'
        //             // ],
        //             // [
        //             //     'description'=>'Milling Degree',
        //             //     'standard'=>'Extra Well Milled'
        //             // ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
            
        // ];
       
        if(sizeof($allproduct['data'])>0){
        for ($x = 0; $x < sizeof($allproduct['data']); $x++) {
            $our_products[$x]['src'] = $allproduct['data'][$x]['pd_img_url'] ;
            $our_products[$x]['title'] = $allproduct['data'][$x]['pd_title_en'] ;
            $our_products[$x]['short_description'] = $allproduct['data'][$x]['pd_content_en'] ;
    
            $y = $x;
            if (sizeof($allproduct['data'][$y]['specifications'])==0){
                $our_products[$y]['specification'][0]['description'] = "-";
                $our_products[$y]['specification'][0]['standard'] = "-";
            }
            //if (sizeof($allproduct['data'][$y]['specifications'])>>0){
            for ($i = 0; $i < count(($allproduct['data'][$y]['specifications'])); $i++) {
                //$our_products[$x]['specification'] = $allproduct['data'][$x]['specification'][$i]
                $our_products[$y]['specification'][$i]['description'] = $allproduct['data'][$y]['specifications'][$i]['sp_name_en'];
                //print_r($allproduct['data'][$y]['specifications'][$i]['sp_name_en']);
                //echo $y;
                $our_products[$y]['specification'][$i]['standard'] = $allproduct['data'][$y]['specifications'][$i]['sp_detail_en'];
            //print_r( $allproduct['data'][$x]['pd_img_url']);
            }
            //}
          }
        }
    } elseif ($lang === 'ch'){
        $menu = $menu['ch'];
        $content = [
            'welcome'=>'欢迎来到环球米业的世界',
            'riceland'=>'环球米业有限公司',
            'company_profile'=>$company_profile['ch'],
            'our_vision'=>[
                'title'=>'我们的远景',
                'p1'=>'始终是您在高端产品领域的最佳伙伴'
            ],
            'our_mission'=>$our_mission['ch'],
            'our_products'=>[
                'title'=>'我们的产品',
                'specification'=>'产品标准',
                'description'=>'描述',
                'standard'=>'泰国大米标准'
            ],
            'certificates'=>[
                'title'=>'证书'
            ],
            'form'=>[
                'name'=>'姓名*',
                'email'=>'邮箱*',
                'tel'=>'电话号码*',
                'remark'=>'备注',
                'submit'=> '提交'
            ],
            'how_to_order'=>$how_to_order['ch'],
            'contact_us'=>$contact_us['ch']
        ];
        // $our_products = [
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //             // [
        //             //     'description'=>'Moisture',
        //             //     'standard'=>'14% max'
        //             // ],
        //             // [
        //             //     'description'=>'Whole Kernels',
        //             //     'standard'=>'60% min'
        //             // ],
        //             // [
        //             //     'description'=>'Broken (5.2 mm.)',
        //             //     'standard'=>'4.5% max'
        //             // ],
        //             // [
        //             //     'description'=>'Redkernels',
        //             //     'standard'=>'0%'
        //             // ],
        //             // [
        //             //     'description'=>'Yellow kernels',
        //             //     'standard'=>'0.2% max'
        //             // ],
        //             // [
        //             //     'description'=>'Chalky kernels',
        //             //     'standard'=>'6% max'
        //             // ],
        //             // [
        //             //     'description'=>'Damaged kernels',
        //             //     'standard'=>'0.25% max'
        //             // ],
        //             // [
        //             //     'description'=>'White Glutinous Rice',
        //             //     'standard'=>'1.5% max'
        //             // ],
        //             // [
        //             //     'description'=>'Undeveloped, Immature kernels, Other seeds and Foreign Matter',
        //             //     'standard'=>'0.2% max'
        //             // ],
        //             // [
        //             //     'description'=>'Paddy (grain per 1 kg.)',
        //             //     'standard'=>'7 grains'
        //             // ],
        //             // [
        //             //     'description'=>'Milling Degree',
        //             //     'standard'=>'Extra Well Milled'
        //             // ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
            
        // ];
        
        if(sizeof($allproduct['data'])>0){
        for ($x = 0; $x < sizeof($allproduct['data']); $x++) {
            $our_products[$x]['src'] = $allproduct['data'][$x]['pd_img_url'] ;
            $our_products[$x]['title'] = $allproduct['data'][$x]['pd_title_ch'] ;
            $our_products[$x]['short_description'] = $allproduct['data'][$x]['pd_content_ch'] ;
    
            $y = $x;
            if (sizeof($allproduct['data'][$y]['specifications'])==0){
                $our_products[$y]['specification'][0]['description'] = "-";
                $our_products[$y]['specification'][0]['standard'] = "-";
            }
            //if (sizeof($allproduct['data'][$y]['specifications'])>>0){
            for ($i = 0; $i < count(($allproduct['data'][$y]['specifications'])); $i++) {
                //$our_products[$x]['specification'] = $allproduct['data'][$x]['specification'][$i]
                $our_products[$y]['specification'][$i]['description'] = $allproduct['data'][$y]['specifications'][$i]['sp_name_ch'];
                //print_r(count(array($allproduct['data'][$y]['specifications'])));
                //echo $y;
                $our_products[$y]['specification'][$i]['standard'] = $allproduct['data'][$y]['specifications'][$i]['sp_detail_ch'];
            //print_r( $allproduct['data'][$x]['pd_img_url']);
            }
            //}
          }
        }
    }

    $smarty_instance->assign("lang", $lang);
    $smarty_instance->assign("menu", $menu);
    $smarty_instance->assign("content", $content);
    $smarty_instance->assign("our_products", $our_products);

    $smarty_instance->display('index.tpl');    

?>
