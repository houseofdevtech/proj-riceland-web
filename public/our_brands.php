<?php

    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    $script_path = dirname(__FILE__);
    require_once($script_path.'/object/object_path.php');
    require_once(SMARTY_PATH3);
    require_once(EXAMPLE);
    $brandproduct = require_once('./utils/get_brand.php');
    //print_r($brandproduct);

	$smarty_instance = new Smarty;	
    $smarty_instance->caching = false;	

    // Import Content
    $menu = require_once('./content/menu.php');

    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    }
    else{
        $lang = null;
    }

    if($lang === 'en' || $lang === null){
        $menu = $menu['en'];
        $content = [
            'our_brands'=>[
                'title'=>'OUR BRANDS'
            ]
        ];
        $our_brands = [
            [
                'title'=>'Royal Dancer',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Royal Orchid',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Golden Diamond',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER3',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Royal Orchid',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Royal Orchid',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Royal Orchid',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Royal Orchid',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Royal Orchid',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Royal Orchid',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
            [
                'title'=>'Royal Orchid',
                'items'=>[
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER1',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ],
                    // [
                    //     'src'=>'./img/our_brands/1.jpg',
                    //     'title'=>'BETTER2',
                    //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
                    // ]
                ]
            ],
        ];
        if(sizeof($brandproduct['data'])>0){
        for ($x = 0; $x < sizeof($brandproduct['data']); $x++) {
            $our_brands[$x]['title'] = $brandproduct['data'][$x]['brand_name_en'] ;
            $y = $x;
            //if (sizeof($allproduct['data'][$y]['specifications'])>>0){
            for ($i = 0; $i < count(($brandproduct['data'][$y]['products'])); $i++) {
                //$our_products[$x]['specification'] = $allproduct['data'][$x]['specification'][$i]
                $our_brands[$y]['items'][$i]['src'] = $brandproduct['data'][$y]['products'][$i]['pd_img_url'];
                $our_brands[$y]['items'][$i]['title'] = $brandproduct['data'][$y]['products'][$i]['pd_title_en'];
                $our_brands[$y]['items'][$i]['detail'] = $brandproduct['data'][$y]['products'][$i]['pd_content_en'];
                //print_r(count(array($allproduct['data'][$y]['specifications'])));
                //echo $y;
            //print_r( $allproduct['data'][$x]['pd_img_url']);
            }
            //}
          }
        }

    } elseif ($lang === 'ch'){
        $menu = $menu['ch'];
        $content = [
            'our_brands'=>[
                'title'=>'我们的品牌'
            ]
        ];
        $our_brands = [
            // [
            //     'title'=>'Royal Dancer',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Royal Orchid',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Golden Diamond',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER3',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Royal Orchid',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Royal Orchid',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Royal Orchid',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Royal Orchid',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Royal Orchid',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Royal Orchid',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
            // [
            //     'title'=>'Royal Orchid',
            //     'items'=>[
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER1',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ],
            //         [
            //             'src'=>'./img/our_brands/1.jpg',
            //             'title'=>'BETTER2',
            //             'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            //         ]
            //     ]
            // ],
        ];
        if(sizeof($brandproduct['data'])>0){
        for ($x = 0; $x < sizeof($brandproduct['data']); $x++) {
            $our_brands[$x]['title'] = $brandproduct['data'][$x]['brand_name_ch'] ;
            $y = $x;
            //print_r(count(($brandproduct['data'][1]['products'])));
            //if (sizeof($allproduct['data'][$y]['specifications'])>>0){
            for ($i = 0; $i < count(($brandproduct['data'][$y]['products'])); $i++) {
                
                //$our_products[$x]['specification'] = $allproduct['data'][$x]['specification'][$i]
                $our_brands[$y]['items'][$i]['src'] = $brandproduct['data'][$y]['products'][$i]['pd_img_url'];
                $our_brands[$y]['items'][$i]['title'] = $brandproduct['data'][$y]['products'][$i]['pd_title_ch'];
                $our_brands[$y]['items'][$i]['detail'] = $brandproduct['data'][$y]['products'][$i]['pd_content_ch'];
                //print_r(count(array($allproduct['data'][$y]['specifications'])));
                //echo $y;
            //print_r( $allproduct['data'][$x]['pd_img_url']);
            }
            //}
          }
        }
    }
    
    $smarty_instance->assign("lang", $lang);
    $smarty_instance->assign("menu", $menu);
    $smarty_instance->assign("content", $content);
    $smarty_instance->assign("our_brands", $our_brands);
    $smarty_instance->display('our_brands.tpl');    

?>
