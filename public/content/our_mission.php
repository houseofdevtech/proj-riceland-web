<?php
$our_mission = [
    'en'=>[
        'title'=>'OUR MISSION',
        'p1'=>'We are responsibly to provide high quality product, satisfied service to rice consumer.'
    ],
    'ch'=>[
        'title'=>'我们的使命',
        'p1'=>'为消费者提供优质的产品，与之匹配的高质量的服务'
    ]
];
return $our_mission;
?>