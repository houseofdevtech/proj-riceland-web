<?php
$menu = [
    'en'=>[
        'home'=>'Home',
        'about_us'=>'About us',
        'our_products'=>'Our Products',
        'our_brands'=>'Our Brands',
        'contact_us'=>'Contact us',
        'footer'=>'Additional menu'
    ],
    'ch'=>[
        'home'=>'主页',
        'about_us'=>'关于我们',
        'our_products'=>'我们的产品',
        'our_brands'=>'我们的品牌',
        'contact_us'=>'联系我们',
        'footer'=>'Additional menu'
    ]
];
return $menu;
?>