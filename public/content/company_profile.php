<?php
$company_profile = [
    'en'=>[
        'title'=>'COMPANY PROFILE',
        'p1'=>'Riceland Foods Ltd. was founded in 1982 by Mr. San Chuapanyavit and rice factory was established in 1986 locate in Saraburi province which is the gateway to the northeastern region of Thailand. ',
        'p12'=>'Since 1982, Riceland Foods Limited has gained trust and confidence of customers worldwide. As one of the leading Thai Rice exporter, we always focused on producing the premium Thai rice to our client all over the world. ',
        'p2'=>'Our main product is Thai Hom Mali Rice , which has been awarded as “ The World’s best Rice “ for many years. In 2016, we as the first exporter own Chinese organic certificate and had successfully export Organic Thai rice to Chinese market.',
        'p22'=>'We take the responsibility to select the high-quallity, healthy, green and organic Thai rice to our client worldwide, therefore, we are continued to improve, expand and keep update our equipment to match the market trend. ',
        'p23'=>'Our first constant temperature warehouse was built in 2018, it keep the age of rice and pack by cauum that make it as the same whole year for our premium product. '
    ],
    'ch'=>[
        'title'=>'公司简介',
        'p1'=>'环球米业有限公由徐位合先生创立于1982年，于1986年正式建立环球米业有限公司大米加工厂，位于通往东北部门户的北标府。',
        'p12'=>'创立至今，环球始终专注于高端泰国大米的生产及出口，并把泰国优质大米引入全球多个国家，深得各国人民的喜爱。 ',
        'p2'=>'环球产品以荣获多年世界最佳大米的泰国茉莉香米为主，且多年来专注于推广泰国有机米，并于2016年作为首家拥有中国有机证书的出口商，成功把泰国有机米引入中国市场并获得广大消费者的认可。 在挑选优质、健康、绿色、有机的泰国大米的路上，环球米业从未敢松懈，并深知其重任，从而不断的提升，扩张，与时俱进，使得环球米业一直走在大米出口以及产品创新的前沿。 ',
        'p22'=>'2018年第一个恒温仓完工并投入使用，开始从源头上对大米进行冻龄，再结合成熟的真空生产线，为高端优质品牌全年保鲜。',
        'p23'=>''
        
    ]
];
return $company_profile;
?>