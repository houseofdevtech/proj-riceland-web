<?php
$contact_us = [
    'en'=>[
        'title'=>'CONTACT US',
        'sub_title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.',
        'address'=>[
            'addr_1'=>'77/140 33rd Floor Sinn Sathorn Tower,',
            'addr_2'=>'Krungthonburi Rd, Klongsan, Bangkok, 10600, Thailand ',
        ],
        'telephone'=>[
            'tel'=>'+66(0) 2 4400 855-8',
            'fax'=>'Fax: +66(0) 4400880, 4400910'
        ],
        'email'=>'rice1@riceland.co.th'
    ],
    'ch'=>[
        'title'=>'联系我们',
        'sub_title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.',
        'address'=>[
            'addr_1'=>'泰国曼谷库屯武里新沙吞大厦33层77/140号',
            'addr_2'=>'',
        ],
        'telephone'=>[
            'tel'=>'+66(0) 2 4400 855-8',
            'fax'=>'Fax: +66(0) 4400880, 4400910'
        ],
        'email'=>'rice1@riceland.co.th'
    ]
];
return $contact_us;
?>