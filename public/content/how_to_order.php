<?php
$how_to_order = [
    'en'=>[
        'title'=>'HOW TO ORDER',
        'steps'=>[
            [
                'src'=>'./img/how_to_order/1.svg',
                'heading'=>'1. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
            [
                'src'=>'./img/how_to_order/2.svg',
                'heading'=>'2. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
            [
                'src'=>'./img/how_to_order/3.svg',
                'heading'=>'3. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
            [
                'src'=>'./img/how_to_order/4.svg',
                'heading'=>'4. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
            [
                'src'=>'./img/how_to_order/5.svg',
                'heading'=>'5. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
        ]
    ],
    'ch'=>[
        'title'=>'HOW TO ORDER',
        'steps'=>[
            [
                'src'=>'./img/how_to_order/1.svg',
                'heading'=>'1. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
            [
                'src'=>'./img/how_to_order/2.svg',
                'heading'=>'2. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
            [
                'src'=>'./img/how_to_order/3.svg',
                'heading'=>'3. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
            [
                'src'=>'./img/how_to_order/4.svg',
                'heading'=>'4. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
            [
                'src'=>'./img/how_to_order/5.svg',
                'heading'=>'5. Lorem ipsum dolor sit amet',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ],
        ]
    ]
];
return $how_to_order;
?>