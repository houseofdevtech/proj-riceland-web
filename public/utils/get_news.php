<?php

// $current_url = $_SERVER['REQUEST_URI'];
$_SESSION['page']=$_GET['page'];
$page= $_SESSION['page'];
$url = 'https://content.houseofdev.tech/riceland/api/news_page/';
$url .= $page;

//echo $current_url;
$curl = curl_init();


curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);

curl_close($curl);
//echo $response;
return json_decode($response,true);