<?php

    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    $script_path = dirname(__FILE__);
    require_once($script_path.'/object/object_path.php');
    require_once(SMARTY_PATH3);
    require_once(EXAMPLE);
    require_once("./utils/session.php");
    $newsapi = require_once("./utils/get_each_news.php");
    //var_dump($newsapi);
	$smarty_instance = new Smarty;	
    $smarty_instance->caching = false;	

    // Import Content
    $menu = require_once('./content/menu.php');

    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    }
    else{
        $lang = null;
    }

    if($lang === 'en' || $lang === null){
        $menu = $menu['en'];
        $content = [
            'go_back'=>'GO BACK'
        ];
        $post = [
            'src'=>'./img/about_us/news_and_csr/img-1.jpg',
            'date'=>'12/2/2021',
            'title'=>'Lorem ipsum dolor sit amet',
            'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque amet, massa magna dui, sit cursus tristique. Sollicitudin dolor, tortor dolor maecenas enim tincidunt. Proin consequat scelerisque quam leo. Dolor orci, fringilla cum sit orci, lacus, suspendisse tincidunt. Sed ac diam mauris commodo id quam malesuada. Ut ultricies phasellus hendrerit eget dignissim porttitor. Mattis suscipit vitae massa enim condimentum.'
        ];
        if($newsapi['success']===true){
            $post['src'] = $newsapi['data']['news_img_url'] ;
            $post['date'] = date("d/m/Y", strtotime($newsapi['data']['news_date']));
            $post['title'] = $newsapi['data']['news_title_en'] ;
            $post['detail'] = $newsapi['data']['news_content_en'] ;
        }

    } elseif ($lang === 'ch'){
        $menu = $menu['ch'];
        $content = [
            'go_back'=>'GO BACK'
        ];
        $post = [
            'src'=>'./img/about_us/news_and_csr/img-1.jpg',
            'date'=>'12/2/2021',
            'title'=>'Lorem ipsum dolor sit amet',
            'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque amet, massa magna dui, sit cursus tristique. Sollicitudin dolor, tortor dolor maecenas enim tincidunt. Proin consequat scelerisque quam leo. Dolor orci, fringilla cum sit orci, lacus, suspendisse tincidunt. Sed ac diam mauris commodo id quam malesuada. Ut ultricies phasellus hendrerit eget dignissim porttitor. Mattis suscipit vitae massa enim condimentum.'
        ];
        if($newsapi['success']===true){
        $post['src'] = $newsapi['data']['news_img_url'] ;
        $post['date'] = date("d/m/Y", strtotime($newsapi['data']['news_date']));
        $post['title'] = $newsapi['data']['news_title_ch'] ;
        $post['detail'] = $newsapi['data']['news_content_ch'] ;
        }
    }

    $smarty_instance->assign("lang", $lang);
    $smarty_instance->assign("menu", $menu);
    $smarty_instance->assign("content", $content);
    $smarty_instance->assign("post", $post);

    $smarty_instance->display('news_and_csr_post.tpl');    

?>
