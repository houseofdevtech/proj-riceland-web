<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <title>Riceland</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta
      name="viewport"
      content="user-scalable=no, width=device-width, height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1, minimal-ui"
    />

    <meta name="theme-color" content="#4A8B71" />
    <meta name="msapplication-navbutton-color" content="#4A8B71" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#4A8B71" />

    <!-- Favicons
		================================================== -->
    <link rel="shortcut icon" href="./img/favicon.ico" />
    <link rel="apple-touch-icon" href="./img/apple-touch-icon.png" />
    <link
      rel="apple-touch-icon"
      sizes="72x72"
      href="./img/apple-touch-icon-72x72.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="114x114"
      href="./img/apple-touch-icon-114x114.png"
    />

    <!-- Font Awesome
		================================================== -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
      crossorigin="anonymous"
    />

    <!-- CSS
		================================================== -->
    <link rel="stylesheet" href="./css/style.min.css" type="text/css" />

    <!-- Load google font
		================================================== -->
    <script type="text/javascript">
      WebFontConfig = {
        google: {
          families: [
            "Mitr:200,300,400,500,600,700",
            "Poppins:300,400,500,600,700",
            //"Raleway:400,400i,500,500i,700,700i",
          ],
        },
      };
      (function () {
        var wf = document.createElement("script");
        wf.src =
          ("https:" == document.location.protocol ? "https" : "http") +
          "://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js";
        wf.type = "text/javascript";
        wf.async = "true";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(wf, s);
      })();
    </script>

    <!-- Load other scripts
		================================================== -->
    <script type="text/javascript">
      var _html = document.documentElement;
      _html.className = _html.className.replace("no-js", "js");
    </script>
    <script type="text/javascript" src="./js/device.min.js"></script>

    <!-- Map Chart
		================================================== -->
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/maps.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/geodata/worldLow.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

    <!-- Sweet Alert
		================================================== -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link
      href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css"
      rel="stylesheet"
    />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  </head>
</html>

<body class="page page-blog">
  <!-- start top bar -->
  
  <div id="top-bar" class="top-bar--style-2">
    <div class="container">
      <a id="top-bar__logo"{if $lang=="ch"} class="ch-logo" {/if} class="site-logo" href="index.php{if isset($lang)}?lang={$lang}{/if}"></a>

      <a id="top-bar__navigation-toggler" href="javascript:void(0);"
        ><span></span
      ></a>

      <nav id="top-bar__navigation" role="navigation">
        <ul>
          <!-- ANCHOR Nav-Home -->
          <li ><a href="index.php{if isset($lang)}?lang={$lang}{/if}">{$menu.home}</a></li>

          <!-- <li><a href="index.html">Home</a></li> -->

          <!-- ANCHOR Nav-About-us -->
          <li class="current"><a href="about_us.php{if isset($lang)}?lang={$lang}{/if}">{$menu.about_us}</a></li>

          <!-- <li><a href="about_us.html">About us</a></li> -->

          <!-- ANCHOR Nav-Products -->
          <li >
  					<a href="our_products.php{if isset($lang)}?lang={$lang}{/if}">{$menu.our_products}</a>

  					<div class="submenu">
  						<ul>
  							<li ><a href="our_products.php{if isset($lang)}?lang={$lang}{/if}">{$menu.our_products}</a></li>
  							<li ><a href="our_brands.php{if isset($lang)}?lang={$lang}{/if}">{$menu.our_brands}</a></li>
  						</ul>
  					</div>
  				</li>

          <!-- <li>
            <a href="javascript:void(0);">Our products</a>

            <div class="submenu">
              <ul>
                <li><a href="our_products.html">Our products</a></li>
                <li><a href="our_brands.html">Our brands</a></li>
              </ul>
            </div>
          </li> -->

          <!-- ANCHOR Nav-Contact -->
          <li ><a href="contact_us.php{if isset($lang)}?lang={$lang}{/if}">{$menu.contact_us}</a></li>
        
          <!-- <li><a href="contact_us.html">Contacts</a></li> -->

          <!-- ANCHOR Nav-Language -->
          <li class="mt-5 mt-md-0 d-flex">
            <a type="button" style="border-right: 2px solid #c4c4c4;" onclick="changeLanguage('en')">EN</a>
            <a type="button" onclick="changeLanguage('ch')">CH</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  <!-- end top bar -->

  <!-- start header -->
  <header
    class="intro align-items-center jarallax"
    data-speed="0.5"
    data-img-position="50% 50%"
    style="background-image: url({$post.src})"
  >
    <!-- <div class="pattern" style="opacity: 0.25"></div> -->

    <!-- <div class="container">
      <div class="intro__text">
        <p class="intro__subtitle">Fruits</p>
        <h1 class="intro__title">Fresh grape</h1>
        <p class="intro__post-date">August 4, 2016 | 4 Comments</p>
      </div>
    </div> -->
  </header>
  <!-- end header -->

  <main id="news-and-csr-post" role="main">
    <!-- SECTION - Post -->
    <section class="section bg-grey">
      <div class="container">
        <div class="section-heading section-heading--left mb-md-5">
          <!-- ANCHOR - Date -->
          <p class="date text-grey">{$post.date}</p>
          <!-- ANCHOR - Title -->
          <h2 class="title responsive-title text-primary">
            {$post.title}
          </h2>
        </div>
        <!-- ANCHOR - Detail -->
        <p class="my-0">
          {$post.detail}
        </p>
        <!-- ANCHOR - Go Back Button -->
        <div class="d-flex justify-content-center mt-5 pt-3 pt-md-5">
          <button class="custom-btn primary filled" onclick="goBack()">
            {$content.go_back}
          </button>
        </div>
      </div>
    </section>
    <!-- !SECTION - End Post -->
  </main>

  <!-- start footer -->
  <footer id="footer" class="footer--style-1">
    <div class="footer__inner">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-6">
            <div class="footer__item">
              <a
                class="site-logo {if $lang==='ch'}ch-logo{/if}"
                href="index.php{if isset($lang)}?lang={$lang}{/if}"
                ></a
              >

              <p class="footer__copy">© 2021, Riceland. All rights reserved.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-6">
            <div class="footer__item">
              <h3 class="footer__title">
                <!-- {$menu.footer} -->
              </h3>

              <div class="row">
                <div class="col">
                  <ul class="footer__menu">
                    <li>
                      <a href="index.php{if isset($lang)}?lang={$lang}{/if}"
                        >{$menu.home}</a
                      >
                    </li>
                    <li>
                      <a
                        href="our_products.php{if isset($lang)}?lang={$lang}{/if}"
                        >{$menu.our_products}</a
                      >
                    </li>
                  </ul>
                </div>

                <div class="col">
                  <ul class="footer__menu">
                    <li>
                      <a href="about_us.php{if isset($lang)}?lang={$lang}{/if}"
                        >{$menu.about_us}</a
                      >
                    </li>
                    <li>
                      <a href="contact_us.php{if isset($lang)}?lang={$lang}{/if}"
                        >{$menu.contact_us}</a
                      >
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="col-md-12 col-lg-4">
  					<div class="footer__item">
  						<h3 class="footer__title">Get a newslatter</h3>

  						<form class="footer__form form-horizontal" action="#">
  							<p>Effervescent the secure special. Kids spicey chance excellent proven too sleek handcrafted when makes intense</p>

  							<div class="b-table">
  								<div class="cell v-bottom">
  									<label class="input-wrp">
  										<input class="textfield" type="text" placeholder="Your E-mail" />
  									</label>
  								</div>

  								<div class="cell v-bottom">
  									<button class="custom-btn primary" type="submit" role="button">subscribe</button>
  								</div>
  							</div>
  						</form>
  					</div>
  				</div> -->
        </div>
      </div>
    </div>
  </footer>

  <!-- end footer -->

  <div id="btn-to-top-wrap">
    <a
      id="btn-to-top"
      class="circled"
      href="javascript:void(0);"
      data-visible-offset="1000"
    ></a>
  </div>

  <!-- Messenger Chat Plugin Code -->
  <div id="fb-root"></div>

  <!-- Your Chat Plugin code -->
  <div id="fb-customer-chat" class="fb-customerchat"></div>

  <script type="text/javascript" src="./js/fb_chat.min.js"></script>

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->
  <script>
    window.jQuery ||
      document.write('<script src="./js/jquery-2.2.4.min.js"><\/script>');
  </script>

  <script type="text/javascript" src="./js/main.min.js"></script>
  <script type="text/javascript" src="./js/modal.min.js"></script>
  <script type="text/javascript" src="./js/change_language.min.js"></script>
  <script type="text/javascript" src="./js/toggle_selection.min.js"></script>
  

  <script>
    function goBack() {
      window.history.back();
    }
  </script>
</body>
</html>