<?php

    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    $script_path = dirname(__FILE__);
    require_once($script_path.'/object/object_path.php');
    require_once(SMARTY_PATH3);
    require_once(EXAMPLE);
    $newsapi = require_once("./utils/get_news_aboutus.php");

	$smarty_instance = new Smarty;	
    $smarty_instance->caching = false;

    // Import Content
    $menu = require_once('./content/menu.php');
    $company_profile = require_once('./content/company_profile.php');
    $our_mission = require_once('./content/our_mission.php');

    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    }
    else{
        $lang = null;
    }

    if($lang === 'en' || $lang === null){
        $menu = $menu['en'];
        $content = [
            'banner_title'=>'ABOUT US',
            'company_profile'=>$company_profile['en'],
            'our_vision'=>[
                'title'=>'OUR VISION',
                'p1'=>'Be your best partner in the High-end rice field'
            ],
            'our_mission'=>$our_mission['en'],
            'export'=>[
                'title'=>'COUNTRIES WE EXPORT'
            ],
            'our_strength'=>[
                'title'=>'OUR STRENGTH',
                'p1'=>'Here are the reasons why Riceland Foods Ltd. (Established since 1982) is one of leading rice exporters of Thailand:',
                'p2'=>'Continual expansion with large market shares in USA, China, and many countries mainly in Europe and Asia. Over 37 years experience in rice export business Top 10 Thai rice exporter – Based on quality of products Own famous rice brands: Royal Dancer, Royal Orchid, Golden Diamond, Golden Globe, Panda, and Falcon Large factory of over 10,000 MT stocking capacity and modern machineries from Japan, England, and Switzerland. GMP, HACCP and BRC accredited'
            ],
            'certificates'=>[
                'title'=>'CERTIFICATES'
            ],
            'news_and_csr'=>[
                'title'=>'NEWS & CSR',
                'see_more'=>'SEE MORE'
            ]
        ];
        $news = [
            'current_page'=>1,
            'total_pages'=>1,
            'news_list'=>[
                // [   
                //     'id'=>'1',
                //     'src'=>'./img/about_us/news_and_csr/img-1.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'2',
                //     'src'=>'./img/about_us/news_and_csr/img-2.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'3',
                //     'src'=>'./img/about_us/news_and_csr/img-3.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'4',
                //     'src'=>'./img/about_us/news_and_csr/img-4.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ]
            ]
        ];
        
        if(sizeof($newsapi['data'])>0){
        $news['current_page'] = $newsapi['current_page'] ;
        $news['total_pages'] = $newsapi['total_page'] ;
        for ($x = 0; $x < sizeof($newsapi['data']); $x++) {
            $news['news_list'][$x]['id'] = $newsapi['data'][$x]['id'] ;
            $news['news_list'][$x]['src'] = $newsapi['data'][$x]['news_img_url'] ;
            $news['news_list'][$x]['date'] = date("d/m/Y", strtotime($newsapi['data'][$x]['news_date']));
            $news['news_list'][$x]['title'] = $newsapi['data'][$x]['news_title_en'] ;
            $news['news_list'][$x]['description'] = $newsapi['data'][$x]['news_content_en'] ;
        }
        }
    } elseif ($lang === 'ch'){
        $menu = $menu['ch'];
        $content = [
            'banner_title'=>'关于我们',
            'company_profile'=>$company_profile['ch'],
            'our_vision'=>[
                'title'=>'我们的远景',
                'p1'=>'始终是您在高端产品领域的最佳伙伴'
            ],
            'our_mission'=>$our_mission['ch'],
            'export'=>[
                'title'=>'主要出口国家'
            ],
            'our_strength'=>[
                'title'=>'我们的优势',
                'p1'=>'Riceland Foods Ltd.（成立于1982 年）成为泰 国 主 要 大 米 出 口 商 之 一 ， 工 厂 荣 获
                GMP、HACCP 和BRC 认证在美国、中国以及主要在欧洲和亚洲的许多国家以较大的市场份额持续扩张。 超过37 年的大米出口业务经验以产品质量为基础 创造了著名品牌：金金仙女、仙兰、金钻石、金球、熊猫和鹰皇。 以可以存储10,000 多吨库存能力和现代化的大型加工厂为保证，加工设备选择来自日本、英国和瑞士的高端先进机械。',
                'p2'=>''
            ],
            'certificates'=>[
                'title'=>'证书'
            ],
            'news_and_csr'=>[
                'title'=>'NEWS & CSR',
                'see_more'=>'SEE MORE'
            ]
        ];
        $news = [
            'current_page'=>1,
            'total_pages'=>6,
            'news_list'=>[
                // [   
                //     'id'=>'1',
                //     'src'=>'./img/about_us/news_and_csr/img-1.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'2',
                //     'src'=>'./img/about_us/news_and_csr/img-2.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'3',
                //     'src'=>'./img/about_us/news_and_csr/img-3.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'4',
                //     'src'=>'./img/about_us/news_and_csr/img-4.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ]
            ]
        ];
        
        if(sizeof($newsapi['data'])>0){
        $news['current_page'] = $newsapi['current_page'] ;
        $news['total_pages'] = $newsapi['total_page'] ;
        for ($x = 0; $x < sizeof($newsapi['data']); $x++) {
            $news['news_list'][$x]['id'] = $newsapi['data'][$x]['id'] ;
            $news['news_list'][$x]['src'] = $newsapi['data'][$x]['news_img_url'] ;
            $news['news_list'][$x]['date'] = date("d/m/Y", strtotime($newsapi['data'][$x]['news_date']));
            $news['news_list'][$x]['title'] = $newsapi['data'][$x]['news_title_ch'] ;
            $news['news_list'][$x]['description'] = $newsapi['data'][$x]['news_content_ch'] ;
        }
        }
    }

    $smarty_instance->assign("lang", $lang);
    $smarty_instance->assign("menu", $menu);
    $smarty_instance->assign("content", $content);
    $smarty_instance->assign("news", $news);

    $smarty_instance->display('about_us.tpl');    

?>