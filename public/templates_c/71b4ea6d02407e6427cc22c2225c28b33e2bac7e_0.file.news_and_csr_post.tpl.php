<?php
/* Smarty version 3.1.39, created on 2021-06-18 19:42:33
  from '/Users/tigerpalm/riceland-web/public/templates/news_and_csr_post.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_60cc94b9a61dd1_71954031',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '71b4ea6d02407e6427cc22c2225c28b33e2bac7e' => 
    array (
      0 => '/Users/tigerpalm/riceland-web/public/templates/news_and_csr_post.tpl',
      1 => 1624019889,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60cc94b9a61dd1_71954031 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <title>Riceland</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta
      name="viewport"
      content="user-scalable=no, width=device-width, height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1, minimal-ui"
    />

    <meta name="theme-color" content="#4A8B71" />
    <meta name="msapplication-navbutton-color" content="#4A8B71" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#4A8B71" />

    <!-- Favicons
		================================================== -->
    <link rel="shortcut icon" href="./img/favicon.ico" />
    <link rel="apple-touch-icon" href="./img/apple-touch-icon.png" />
    <link
      rel="apple-touch-icon"
      sizes="72x72"
      href="./img/apple-touch-icon-72x72.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="114x114"
      href="./img/apple-touch-icon-114x114.png"
    />

    <!-- Font Awesome
		================================================== -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
      crossorigin="anonymous"
    />

    <!-- CSS
		================================================== -->
    <link rel="stylesheet" href="./css/style.min.css" type="text/css" />

    <!-- Load google font
		================================================== -->
    <?php echo '<script'; ?>
 type="text/javascript">
      WebFontConfig = {
        google: {
          families: [
            "Mitr:200,300,400,500,600,700",
            "Poppins:300,400,500,600,700",
            //"Raleway:400,400i,500,500i,700,700i",
          ],
        },
      };
      (function () {
        var wf = document.createElement("script");
        wf.src =
          ("https:" == document.location.protocol ? "https" : "http") +
          "://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js";
        wf.type = "text/javascript";
        wf.async = "true";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(wf, s);
      })();
    <?php echo '</script'; ?>
>

    <!-- Load other scripts
		================================================== -->
    <?php echo '<script'; ?>
 type="text/javascript">
      var _html = document.documentElement;
      _html.className = _html.className.replace("no-js", "js");
    <?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="./js/device.min.js"><?php echo '</script'; ?>
>

    <!-- Map Chart
		================================================== -->
    <?php echo '<script'; ?>
 src="https://cdn.amcharts.com/lib/4/core.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdn.amcharts.com/lib/4/maps.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdn.amcharts.com/lib/4/geodata/worldLow.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdn.amcharts.com/lib/4/themes/animated.js"><?php echo '</script'; ?>
>

    <!-- Sweet Alert
		================================================== -->
    <?php echo '<script'; ?>
 src="//cdn.jsdelivr.net/npm/sweetalert2@10"><?php echo '</script'; ?>
>
  </head>
</html>

<body class="page page-blog">
  <!-- start top bar -->
  
  <div id="top-bar" class="top-bar--style-2">
    <div class="container">
      <a id="top-bar__logo" class="site-logo" href="index.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>">Riceland</a>

      <a id="top-bar__navigation-toggler" href="javascript:void(0);"
        ><span></span
      ></a>

      <nav id="top-bar__navigation" role="navigation">
        <ul>
          <!-- ANCHOR Nav-Home -->
          <li ><a href="index.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"><?php echo $_smarty_tpl->tpl_vars['menu']->value['home'];?>
</a></li>

          <!-- <li><a href="index.html">Home</a></li> -->

          <!-- ANCHOR Nav-About-us -->
          <li class="current"><a href="about_us.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"><?php echo $_smarty_tpl->tpl_vars['menu']->value['about_us'];?>
</a></li>

          <!-- <li><a href="about_us.html">About us</a></li> -->

          <!-- ANCHOR Nav-Products -->
          <li >
  					<a href="our_products.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"><?php echo $_smarty_tpl->tpl_vars['menu']->value['our_products'];?>
</a>

  					<div class="submenu">
  						<ul>
  							<li ><a href="our_products.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"><?php echo $_smarty_tpl->tpl_vars['menu']->value['our_products'];?>
</a></li>
  							<li ><a href="our_brands.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"><?php echo $_smarty_tpl->tpl_vars['menu']->value['our_brands'];?>
</a></li>
  						</ul>
  					</div>
  				</li>

          <!-- <li>
            <a href="javascript:void(0);">Our products</a>

            <div class="submenu">
              <ul>
                <li><a href="our_products.html">Our products</a></li>
                <li><a href="our_brands.html">Our brands</a></li>
              </ul>
            </div>
          </li> -->

          <!-- ANCHOR Nav-Contact -->
          <li ><a href="contact_us.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"><?php echo $_smarty_tpl->tpl_vars['menu']->value['contact_us'];?>
</a></li>
        
          <!-- <li><a href="contact_us.html">Contacts</a></li> -->

          <!-- ANCHOR Nav-Language -->
          <li class="mt-5 mt-md-0 d-flex">
            <a type="button" style="border-right: 2px solid #c4c4c4;" onclick="changeLanguage('en')">EN</a>
            <a type="button" onclick="changeLanguage('ch')">CH</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  <!-- end top bar -->

  <!-- start header -->
  <header
    class="intro align-items-center jarallax"
    data-speed="0.5"
    data-img-position="50% 50%"
    style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['post']->value['src'];?>
)"
  >
    <!-- <div class="pattern" style="opacity: 0.25"></div> -->

    <!-- <div class="container">
      <div class="intro__text">
        <p class="intro__subtitle">Fruits</p>
        <h1 class="intro__title">Fresh grape</h1>
        <p class="intro__post-date">August 4, 2016 | 4 Comments</p>
      </div>
    </div> -->
  </header>
  <!-- end header -->

  <main id="news-and-csr-post" role="main">
    <!-- SECTION - Post -->
    <section class="section bg-grey">
      <div class="container">
        <div class="section-heading section-heading--left mb-md-5">
          <!-- ANCHOR - Date -->
          <p class="date text-grey"><?php echo $_smarty_tpl->tpl_vars['post']->value['date'];?>
</p>
          <!-- ANCHOR - Title -->
          <h2 class="title responsive-title text-primary">
            <?php echo $_smarty_tpl->tpl_vars['post']->value['title'];?>

          </h2>
        </div>
        <!-- ANCHOR - Detail -->
        <p class="my-0">
          <?php echo $_smarty_tpl->tpl_vars['post']->value['detail'];?>

        </p>
        <!-- ANCHOR - Go Back Button -->
        <div class="d-flex justify-content-center mt-5 pt-3 pt-md-5">
          <button class="custom-btn primary filled" onclick="goBack()">
            <?php echo $_smarty_tpl->tpl_vars['content']->value['go_back'];?>

          </button>
        </div>
      </div>
    </section>
    <!-- !SECTION - End Post -->
  </main>

  <!-- start footer -->
  <footer id="footer" class="footer--style-1">
    <div class="footer__inner">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-6">
            <div class="footer__item">
              <a
                class="site-logo"
                href="index.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"
                >Riceland</a
              >

              <p class="footer__copy">© 2021, Riceland. All rights reserved.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-6">
            <div class="footer__item">
              <h3 class="footer__title"><?php echo $_smarty_tpl->tpl_vars['menu']->value['footer'];?>
</h3>

              <div class="row">
                <div class="col">
                  <ul class="footer__menu">
                    <li>
                      <a href="index.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"
                        ><?php echo $_smarty_tpl->tpl_vars['menu']->value['home'];?>
</a
                      >
                    </li>
                    <li>
                      <a
                        href="our_products.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"
                        ><?php echo $_smarty_tpl->tpl_vars['menu']->value['our_products'];?>
</a
                      >
                    </li>
                  </ul>
                </div>

                <div class="col">
                  <ul class="footer__menu">
                    <li>
                      <a href="about_us.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"
                        ><?php echo $_smarty_tpl->tpl_vars['menu']->value['about_us'];?>
</a
                      >
                    </li>
                    <li>
                      <a href="contact_us.php<?php if ((isset($_smarty_tpl->tpl_vars['lang']->value))) {?>?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;
}?>"
                        ><?php echo $_smarty_tpl->tpl_vars['menu']->value['contact_us'];?>
</a
                      >
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="col-md-12 col-lg-4">
  					<div class="footer__item">
  						<h3 class="footer__title">Get a newslatter</h3>

  						<form class="footer__form form-horizontal" action="#">
  							<p>Effervescent the secure special. Kids spicey chance excellent proven too sleek handcrafted when makes intense</p>

  							<div class="b-table">
  								<div class="cell v-bottom">
  									<label class="input-wrp">
  										<input class="textfield" type="text" placeholder="Your E-mail" />
  									</label>
  								</div>

  								<div class="cell v-bottom">
  									<button class="custom-btn primary" type="submit" role="button">subscribe</button>
  								</div>
  							</div>
  						</form>
  					</div>
  				</div> -->
        </div>
      </div>
    </div>
  </footer>

  <!-- end footer -->

  <div id="btn-to-top-wrap">
    <a
      id="btn-to-top"
      class="circled"
      href="javascript:void(0);"
      data-visible-offset="1000"
    ></a>
  </div>

  <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
>
    window.jQuery ||
      document.write('<?php echo '<script'; ?>
 src="./js/jquery-2.2.4.min.js"><\/script>');
  <?php echo '</script'; ?>
>

  <?php echo '<script'; ?>
 type="text/javascript" src="./js/main.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 type="text/javascript" src="./js/modal.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 type="text/javascript" src="./js/change_language.min.js"><?php echo '</script'; ?>
>
  

  <?php echo '<script'; ?>
>
    function goBack() {
      window.history.back();
    }
  <?php echo '</script'; ?>
>
</body>
</html><?php }
}
