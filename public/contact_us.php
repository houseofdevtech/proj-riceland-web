<?php

    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    $script_path = dirname(__FILE__);
    require_once($script_path.'/object/object_path.php');
    require_once(SMARTY_PATH3);
    require_once(EXAMPLE);

	$smarty_instance = new Smarty;	
    $smarty_instance->caching = false;	

    // Import Content
    $menu = require_once('./content/menu.php');
    $how_to_order = require_once('./content/how_to_order.php');
    $contact_us = require_once('./content/contact_us.php');
    $allproduct = require_once('./utils/get_product.php');
    $relatedproduct = require_once('./utils/get_related.php');
    $brandproduct = require_once('./utils/get_brand.php');
    //var_dump($brandproduct);
    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    }
    else{
        $lang = null;
    }
    
    if($lang === 'en' || $lang === null){
        $menu = $menu['en'];
        $content = [
            'how_to_order'=>$how_to_order['en'],
            'contact_us'=>$contact_us['en'],
            'form'=>[
                'name'=>'Your Name*',
                'email'=>'E-mail*',
                'tel'=>'Telephone Number*',
                'remark'=>'Remark',
                'submit'=> 'Submit',
                'wechat'=>'Wechat',
                'products'=>'Products',
                'size'=>'Size',
                'selectsize'=>'Please select size',
                'mat'=> 'Material of bag',
                'selectmat'=>'Please select material of bag',
                'dest'=>'Destination',
                'country'=>'Country',
                'term'=> 'Term',
                'selectterm'=> 'Please select term',

            ],
        ];
    } elseif ($lang === 'ch'){
        $menu = $menu['ch'];
        $content = [
            'how_to_order'=>$how_to_order['ch'],
            'contact_us'=>$contact_us['ch'],
            'form'=>[
                'name'=>'姓名*',
                'email'=>'邮箱*',
                'tel'=>'电话号码*',
                'remark'=>'备注',
                'submit'=> '提交',
                'wechat'=>'微信',
                'products'=>'产品',
                'size'=>'规格',
                'selectsize'=>'选择规格',
                'mat'=> '包装材质',
                'selectmat'=>'选择包装材质',
                'dest'=>'目的地',
                'country'=>'国家',
                'term'=> '价格模式',
                'selectterm'=> '选择价格模式',
                
            ],
        ];
    }
    
    $smarty_instance->assign("lang", $lang);
    $smarty_instance->assign("menu", $menu);
    $smarty_instance->assign("content", $content);
    $smarty_instance->assign("products", $allproduct['data']);
    $smarty_instance->assign("relatedproduct", $relatedproduct['data']);
    $smarty_instance->assign("brandproduct", $brandproduct['data']);

    $smarty_instance->display('contact_us.tpl');    

?>