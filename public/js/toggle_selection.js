const handleToggleSelectionById = (event, toggleElementId) => {
  let value = Array.from(
    event.target.selectedOptions,
    (option) => option.value
  );

  $(`#${toggleElementId}`).addClass("d-none");
  if (value.includes("others")) $(`#${toggleElementId}`).removeClass("d-none");
};
