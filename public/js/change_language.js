const changeLanguage = (language) => {
  let url = new URL(window.location.href);
  url.searchParams.set("lang", language);
  window.location = url.href;
};
