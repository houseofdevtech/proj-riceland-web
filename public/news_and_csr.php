<?php

    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    $script_path = dirname(__FILE__);
    require_once($script_path.'/object/object_path.php');
    require_once(SMARTY_PATH3);
    require_once(EXAMPLE);
    
    require_once("./utils/session.php");
    $newsapi = require_once("./utils/get_news.php");
    //print_r($news);
    //$page = $_SESSION['page'];
	$smarty_instance = new Smarty;	
    $smarty_instance->caching = false;	

    // Import Content
    $menu = require_once('./content/menu.php');

    if(isset($_GET['page'])){
        $page = $_GET['page'];
    }
    else{
        $page = null;
    }

    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    }
    else{
        $lang = null;
    }

    if($lang === 'en' || $lang === null){
        $menu = $menu['en'];
        $content = [
            'news_and_csr'=>[
                'title'=>'NEWS & CSR'
            ]
        ];
        $news = [
            'current_page'=>1,
            'total_pages'=>2,
            'news_list'=>[
                // [   
                //     'id'=>'1',
                //     'src'=>'./img/about_us/news_and_csr/img-1.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'2',
                //     'src'=>'./img/about_us/news_and_csr/img-2.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'3',
                //     'src'=>'./img/about_us/news_and_csr/img-3.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'4',
                //     'src'=>'./img/about_us/news_and_csr/img-4.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ]
            ]
        ];
        if(sizeof($newsapi['data'])>0){
        $news['current_page'] = $newsapi['current_page'] ;
        $news['total_pages'] = $newsapi['total_page'] ;
        for ($x = 0; $x < sizeof($newsapi['data']); $x++) {
            $news['news_list'][$x]['id'] = $newsapi['data'][$x]['id'] ;
            $news['news_list'][$x]['src'] = $newsapi['data'][$x]['news_img_url'] ;
            $news['news_list'][$x]['date'] = date("d/m/Y", strtotime($newsapi['data'][$x]['news_date']));
            $news['news_list'][$x]['title'] = $newsapi['data'][$x]['news_title_en'] ;
            $news['news_list'][$x]['description'] = $newsapi['data'][$x]['news_content_en'] ;
        }
        }
    } elseif ($lang === 'ch'){
        $menu = $menu['ch'];
        $content = [
            'news_and_csr'=>[
                'title'=>'NEWS & CSR'
            ]
        ];
        $news = [
            'current_page'=>1,
            'total_pages'=>2,
            'news_list'=>[
                // [   
                //     'id'=>'1',
                //     'src'=>'./img/about_us/news_and_csr/img-1.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'2',
                //     'src'=>'./img/about_us/news_and_csr/img-2.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'3',
                //     'src'=>'./img/about_us/news_and_csr/img-3.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [
                //     'id'=>'4',
                //     'src'=>'./img/about_us/news_and_csr/img-4.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [   
                //     'id'=>'5',
                //     'src'=>'./img/about_us/news_and_csr/img-1.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ],
                // [   
                //     'id'=>'6',
                //     'src'=>'./img/about_us/news_and_csr/img-1.jpg',
                //     'date'=>'12/2/2021',
                //     'title'=>'Lorem ipsum dolor sit amet',
                //     'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales faucibus convallis bibendum interdum. Augue eget dignissim eu aliquam lectus ullamcorper facilisi facilisis leo.'
                // ]
            ]
        ];
        if(sizeof($newsapi['data'])>0){
        $news['current_page'] = $newsapi['current_page'] ;
        $news['total_pages'] = $newsapi['total_page'] ;
        for ($x = 0; $x < sizeof($newsapi['data']); $x++) {
            $news['news_list'][$x]['id'] = $newsapi['data'][$x]['id'] ;
            $news['news_list'][$x]['src'] = $newsapi['data'][$x]['news_img_url'] ;
            $news['news_list'][$x]['date'] = date("d/m/Y", strtotime($newsapi['data'][$x]['news_date']));
            $news['news_list'][$x]['title'] = $newsapi['data'][$x]['news_title_ch'] ;
            $news['news_list'][$x]['description'] = $newsapi['data'][$x]['news_content_ch'] ;
        }
        }
    }

    $smarty_instance->assign("lang", $lang);
    $smarty_instance->assign("page", $page);
    $smarty_instance->assign("menu", $menu);
    $smarty_instance->assign("content", $content);
    $smarty_instance->assign("news", $news);

    $smarty_instance->display('news_and_csr.tpl');    

?>
