<?php

    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    $script_path = dirname(__FILE__);
    require_once($script_path.'/object/object_path.php');
    require_once(SMARTY_PATH3);
    require_once(EXAMPLE);
    $allproduct = require_once('./utils/get_product.php');
    $relatedproduct = require_once('./utils/get_related.php');
    //print_r($allproduct);
    // for ($x = 0; $x <= sizeof($allproduct['data']); $x++) {
    //     print_r($all_products['data'][$x]);
    //   }
        //var_dump( $allproduct);
	$smarty_instance = new Smarty;	
    $smarty_instance->caching = false;	

    // Import Content
    $menu = require_once('./content/menu.php');

    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    }
    else{
        $lang = null;
    }

    if($lang === 'en' || $lang === null){
        $menu = $menu['en'];

        $content = [
            'our_products'=>[
                'title'=>'OUR PRODUCTS',
                'specification'=>'Specification of',
                'description'=>'Description',
                'standard'=>'Thai Rice standard'
            ],
            'other_related_products'=>[
                'title'=>'OTHER RELATED PRODUCTS'
            ]
        ];
        // $our_products = [
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //             // [
        //             //     'description'=>'Moisture',
        //             //     'standard'=>'14% max'
        //             // ],
        //             // [
        //             //     'description'=>'Whole Kernels',
        //             //     'standard'=>'60% min'
        //             // ],
        //             // [
        //             //     'description'=>'Broken (5.2 mm.)',
        //             //     'standard'=>'4.5% max'
        //             // ],
        //             // [
        //             //     'description'=>'Redkernels',
        //             //     'standard'=>'0%'
        //             // ],
        //             // [
        //             //     'description'=>'Yellow kernels',
        //             //     'standard'=>'0.2% max'
        //             // ],
        //             // [
        //             //     'description'=>'Chalky kernels',
        //             //     'standard'=>'6% max'
        //             // ],
        //             // [
        //             //     'description'=>'Damaged kernels',
        //             //     'standard'=>'0.25% max'
        //             // ],
        //             // [
        //             //     'description'=>'White Glutinous Rice',
        //             //     'standard'=>'1.5% max'
        //             // ],
        //             // [
        //             //     'description'=>'Undeveloped, Immature kernels, Other seeds and Foreign Matter',
        //             //     'standard'=>'0.2% max'
        //             // ],
        //             // [
        //             //     'description'=>'Paddy (grain per 1 kg.)',
        //             //     'standard'=>'7 grains'
        //             // ],
        //             // [
        //             //     'description'=>'Milling Degree',
        //             //     'standard'=>'Extra Well Milled'
        //             // ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
            
        // ];
   // print_r($allproduct['data'][1]['specifications'][1]['sp_name_en']);
    //  echo sizeof($allproduct['data'][1]['specifications']);
    
    if(sizeof($allproduct['data'])>0){
    for ($x = 0; $x < sizeof($allproduct['data']); $x++) {
        $our_products[$x]['src'] = $allproduct['data'][$x]['pd_img_url'] ;
        $our_products[$x]['title'] = $allproduct['data'][$x]['pd_title_en'] ;
        $our_products[$x]['short_description'] = $allproduct['data'][$x]['pd_content_en'] ;

        $y = $x;
        if (sizeof($allproduct['data'][$y]['specifications'])==0){
            $our_products[$y]['specification'][0]['description'] = "-";
            $our_products[$y]['specification'][0]['standard'] = "-";
        }
        //if (sizeof($allproduct['data'][$y]['specifications'])>>0){
        for ($i = 0; $i < count(($allproduct['data'][$y]['specifications'])); $i++) {
            //$our_products[$x]['specification'] = $allproduct['data'][$x]['specification'][$i]
            $our_products[$y]['specification'][$i]['description'] = $allproduct['data'][$y]['specifications'][$i]['sp_name_en'];
            //print_r(count(array($allproduct['data'][$y]['specifications'])));
            //echo $y;
            $our_products[$y]['specification'][$i]['standard'] = $allproduct['data'][$y]['specifications'][$i]['sp_detail_en'];
        //print_r( $allproduct['data'][$x]['pd_img_url']);
        }
        //}
      }
    }

        $related_products = [
            // [
            //     'src'=>'./img/our_brands/1.jpg',
            //     'title'=>'BETTER1',
            //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            // ],
            // [
            //     'src'=>'./img/our_brands/1.jpg',
            //     'title'=>'BETTER2',
            //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            // ],
            // [
            //     'src'=>'./img/our_brands/1.jpg',
            //     'title'=>'BETTER3',
            //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            // ],
            // [
            //     'src'=>'./img/our_brands/1.jpg',
            //     'title'=>'BETTER4',
            //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            // ]
        ];
        if(sizeof($relatedproduct['data'])>0){
        for ($x = 0; $x < sizeof($relatedproduct['data']); $x++) {
            $related_products[$x]['src'] = $relatedproduct['data'][$x]['pd_img_url'] ;
            $related_products[$x]['title'] = $relatedproduct['data'][$x]['pd_title_en'] ;
            $related_products[$x]['detail'] = $relatedproduct['data'][$x]['pd_content_en'] ;
        }
        }


    } elseif ($lang === 'ch'){
        $menu = $menu['ch'];
        $content = [
            'our_products'=>[
                'title'=>'我们的产品',
                'specification'=>'产品标准',
                'description'=>'描述',
                'standard'=>'泰国大米标准'
            ],
            'other_related_products'=>[
                'title'=>'其他相关产品'
            ]
        ];
        // $our_products = [
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //             // [
        //             //     'description'=>'Moisture',
        //             //     'standard'=>'14% max'
        //             // ],
        //             // [
        //             //     'description'=>'Whole Kernels',
        //             //     'standard'=>'60% min'
        //             // ],
        //             // [
        //             //     'description'=>'Broken (5.2 mm.)',
        //             //     'standard'=>'4.5% max'
        //             // ],
        //             // [
        //             //     'description'=>'Redkernels',
        //             //     'standard'=>'0%'
        //             // ],
        //             // [
        //             //     'description'=>'Yellow kernels',
        //             //     'standard'=>'0.2% max'
        //             // ],
        //             // [
        //             //     'description'=>'Chalky kernels',
        //             //     'standard'=>'6% max'
        //             // ],
        //             // [
        //             //     'description'=>'Damaged kernels',
        //             //     'standard'=>'0.25% max'
        //             // ],
        //             // [
        //             //     'description'=>'White Glutinous Rice',
        //             //     'standard'=>'1.5% max'
        //             // ],
        //             // [
        //             //     'description'=>'Undeveloped, Immature kernels, Other seeds and Foreign Matter',
        //             //     'standard'=>'0.2% max'
        //             // ],
        //             // [
        //             //     'description'=>'Paddy (grain per 1 kg.)',
        //             //     'standard'=>'7 grains'
        //             // ],
        //             // [
        //             //     'description'=>'Milling Degree',
        //             //     'standard'=>'Extra Well Milled'
        //             // ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
        //     [
        //         'src'=>'./img/our_products/img_1.jpg',
        //         'title'=>'Thai Jasmine Rice',
        //         'short_description'=>'Thai Hom Mali Rice has a natural fragrance in itself, especially when cooked. Also it is famous for its unique appearance, texture and aroma.Thai Hom Mali Rice is pure white, springy and so soft to the taste at the same time.',
        //         'specification' => [
        //             [
        //                 'description'=>'-',
        //                 'standard'=>'-'
        //             ],
        //         ]
        //     ],
            
        // ];
        
        if(sizeof($allproduct['data'])>0){
        for ($x = 0; $x < sizeof($allproduct['data']); $x++) {
            $our_products[$x]['src'] = $allproduct['data'][$x]['pd_img_url'] ;
            $our_products[$x]['title'] = $allproduct['data'][$x]['pd_title_ch'] ;
            $our_products[$x]['short_description'] = $allproduct['data'][$x]['pd_content_ch'] ;
    
            $y = $x;
            if (sizeof($allproduct['data'][$y]['specifications'])==0){
                $our_products[$y]['specification'][0]['description'] = "-";
                $our_products[$y]['specification'][0]['standard'] = "-";
            }
            for ($i = 0; $i < count(($allproduct['data'][$y]['specifications'])); $i++) {
                //$our_products[$x]['specification'] = $allproduct['data'][$x]['specification'][$i]
                $our_products[$y]['specification'][$i]['description'] = $allproduct['data'][$y]['specifications'][$i]['sp_name_ch'];
                //print_r(count(array($allproduct['data'][$y]['specifications'])));
                //echo $y;
                $our_products[$y]['specification'][$i]['standard'] = $allproduct['data'][$y]['specifications'][$i]['sp_detail_ch'];
            //print_r( $allproduct['data'][$x]['pd_img_url']);
            }
            //}
          }
        }
        
        $related_products = [
            // [
            //     'src'=>'./img/our_brands/1.jpg',
            //     'title'=>'BETTER1',
            //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            // ],
            // [
            //     'src'=>'./img/our_brands/1.jpg',
            //     'title'=>'BETTER2',
            //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            // ],
            // [
            //     'src'=>'./img/our_brands/1.jpg',
            //     'title'=>'BETTER3',
            //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            // ],
            // [
            //     'src'=>'./img/our_brands/1.jpg',
            //     'title'=>'BETTER4',
            //     'detail'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis morbi turpis at elit pulvinar velit. Orci ornare aliquet odio lacinia. Lectus lacus, varius tincidunt pulvinar fringilla cursus erat. Pellentesque sit nisl ultricies egestas et facilisis blandit.'
            // ]
        ];
        
        if(sizeof($relatedproduct['data'])>0){
        for ($x = 0; $x < sizeof($relatedproduct['data']); $x++) {
            $related_products[$x]['src'] = $relatedproduct['data'][$x]['pd_img_url'] ;
            $related_products[$x]['title'] = $relatedproduct['data'][$x]['pd_title_ch'] ;
            $related_products[$x]['detail'] = $relatedproduct['data'][$x]['pd_content_ch'] ;
        }
        }
    }
    //var_dump (sizeof($allproduct['data']));
    $smarty_instance->assign("lang", $lang);
    $smarty_instance->assign("menu", $menu);
    $smarty_instance->assign("content", $content);
    $smarty_instance->assign("our_products", $our_products);
    $smarty_instance->assign("related_products", $related_products);

    $smarty_instance->display('our_products.tpl');    

?>
